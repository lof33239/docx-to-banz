﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordZuBundesanzeigerCore;
using Path = System.IO.Path;

namespace WordZuBundesanzeigerTest.testutil
{
    class TestDoc: IDisposable
    {
        public string Filename {get;}
        private WordprocessingDocument Doc;

        public TestDoc()
        {
            Filename = "___"+Path.GetRandomFileName();
            new TemplateGen().CreateTemplate(Filename);
        }

        public WordprocessingDocument GetDocument()
        {
            Doc = WordprocessingDocument.Open(Filename, false);
            return Doc;
        }

        ~TestDoc()
        {
            Dispose();
        }

        public void AppendParagraph(string style, string message)
        {
            using (Doc = WordprocessingDocument.Open(Filename, true))
            {
                Doc.MainDocumentPart.Document.Body.AppendChild(CreateParagraph(style, message));
            }
        }

        private Paragraph CreateParagraph(string style, string message)
        {
            Text t = new Text(message);
            Run r = new Run(t);

            Paragraph p = new Paragraph(r);

            ParagraphProperties pPr = new ParagraphProperties
            {
                ParagraphStyleId = new ParagraphStyleId() { Val = style }
            };

            p.PrependChild(pPr);

            return p;
        }

        public static TestDoc GetMinimalTestDoc()
        {
            TestDoc t = new TestDoc();
            t.AppendParagraph("Titel", "Titel");
            return t;
        }

        public void AppendSimpleTable(string[,,] td)
        {
            Table table = new Table();

            TableGrid tableGrid = new TableGrid();

            table.Append(tableGrid);

            for (int i = 0; i < td.GetLength(1); i++)
            {
                GridColumn gridColumn = new GridColumn();

                tableGrid.Append(gridColumn);
            }

            for (int i = 0; i < td.GetLength(0); i++)
            {
                TableRow tableRow = new TableRow();

                table.Append(tableRow);
                for (int j = 0; j < td.GetLength(1); j++)
                {
                    TableCell tableCell = new TableCell();

                    tableCell.Append(CreateParagraph(td[i,j,0], td[i,j,1]));

                    tableRow.Append(tableCell);
                }
            }

            using (Doc = WordprocessingDocument.Open(Filename, true))
            {
                Doc.MainDocumentPart.Document.Body.Append(table);
            }
        }

        public void Dispose()
        {
            Doc.Dispose();
            File.Delete(Filename);
        }
    }
}
