using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WordZuBundesanzeigerTest.testutil
{
    class Util
    {
        public static string MakeWhitespaceIndifferent(string s)
        {
            Regex r = new Regex(@"\s+");
            s = r.Replace(s, " ");
            return s;
        }

        public static string GetTestStringFromFile(string Filepath)
        {
            string text = File.OpenText(Filepath).ReadToEnd();
            return Util.MakeWhitespaceIndifferent(text);
        }

        public static string GetStringFromXDocument(XDocument document)
        {
            string text = document.ToString();
            return Util.MakeWhitespaceIndifferent(text);
        }

        public static string GetCorrespondingXmlFilepath(string Filepath)
        {
            return Filepath + "__xml__.xml";
        }
    }
}
