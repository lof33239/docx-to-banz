﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordZuBundesanzeigerCore;

namespace WordZuBundesanzeigerTest.testutil
{
    class TestXml
    {
        public static BundesanzeigerXmlCreator CreatorWithSampleDocument()
        {
            var ret = new BundesanzeigerXmlCreator("Firmenname", "Firmensitz", XmlVariant.Bundesanzeiger);
            ret.AddToHead(ret.CreateXElement("Titel", "Titel"));
            return ret;
        }
    }
}
