﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WordZuBundesanzeigerCore;

namespace WordZuBundesanzeigerTest.testutil
{
    class Components
    {
        public static string[,,] getSimpleTableString()
        {
           var ret =  new string[,,]
            {
                {{"THEAD", "heading1"},{"THEAD", "heading2"},{"THEAD", "heading3"}},
                {{"TBODY", "body 1,1"},{"TBODY", "body  1,2"},{"TBODY", "body  1,3"}},
                {{"TBODY", "body 2,1"},{"TBODY", "body  2,2"},{"TBODY", "body  2,3"}},
                {{"TBODY", "body 3,1"},{"TBODY", "body  3,2"},{"TBODY", "body  3,3"}},
                {{"TBODY", "body 4,1"},{"TBODY", "body  4,2"},{"TBODY", "body  4,3"}},
            };
           return ret;
        }

        private static XElement td(string text, int colspan = 1, string align = "left")
        {
            return new XElement("TD", text, new XAttribute(BanzXml.ATTR_COLSPAN, colspan), new XAttribute(BanzXml.ATTR_ALIGN, align));
        }

        private static XElement col(int width)
        {
            return new XElement("COL", new XAttribute(BanzXml.ATTR_WIDTH, $"{width}%"));
        }

        public static XElement GetSimpleXMLTable()
        {

            return new XElement("TABLE",
                col(33),
                col(33),
                col(33),
                new XElement("THEAD",
                    new XElement("TR",td("heading1"), td( "heading2"), td( "heading3"))
                ),
                new XElement("TBODY",
                    new XElement("TR",td( "body 1,1"), td( "body 1,2"), td( "body 1,3")),
                    new XElement("TR",td( "body 2,1"), td( "body 2,2"), td( "body 2,3")),
                    new XElement("TR",td( "body 3,1"), td( "body 3,2"), td( "body 3,3")),
                    new XElement("TR",td( "body 4,1"), td( "body 4,2"), td( "body 4,3"))
                )
            );
        }
    }
}
