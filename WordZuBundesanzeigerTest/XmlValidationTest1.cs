﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Schema;
using WordZuBundesanzeigerCore;
using WordZuBundesanzeigerTest.testutil;

namespace WordZuBundesanzeigerTest
{
    [TestClass]
    public class XmlValidationTest1
    {
        [TestMethod]
        public void XmlWithMissingTitleShouldNotValidate()
        {
            using(var t = new TestDoc())
            {
                Assert.ThrowsException<XmlSchemaValidationException>(() => new Parser(new MockLogger(), "Firmenname", "Firmensitz", XmlVariant.Bundesanzeiger, t.GetDocument()).ConvertDocxToXmlWithValidation());
            }
        }

        [TestMethod]
        public void XmlWithOnlyTitleShouldValidate()
        {
            using (var t = new TestDoc())
            {
                t.AppendParagraph("Titel", "Titel");
                new Parser(new MockLogger(), "Firmenname", "Firmensitz", XmlVariant.Bundesanzeiger,t.GetDocument()).ConvertDocxToXmlWithValidation();
            }
        }
    }
}
