﻿using Light.GuardClauses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using WordZuBundesanzeigerCore;

namespace WordZuBundesanzeigerTest
{
    [TestClass]
    public sealed class BundesanzeigerXmlCreatorTests
    {
        [TestMethod]
        public void CheckDocumentAfterConstructor()
        {
            var instance = new BundesanzeigerXmlCreator("Foo", "Bar", XmlVariant.Bundesanzeiger);

            var actualXml = instance.ToString();

            var expectedXml = File.ReadAllText("CreatorAfterConstructor.xml");
            Assert.IsTrue(expectedXml.Equals(actualXml, StringComparisonType.OrdinalIgnoreWhiteSpace));
        }

    }
}
