﻿using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Xml.Linq;
using DocumentFormat.OpenXml.Packaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordZuBundesanzeigerCore;
using WordZuBundesanzeigerTest.testutil;

namespace WordZuBundesanzeigerTest
{
    [TestClass]
    public class ParserTest1
    {

        [TestMethod]
        public void TestOneParagraphConversion()
        {
            
            TestDoc t = TestDoc.GetMinimalTestDoc();

            t.AppendParagraph("A", "Hallo 1");

            string actual;
            using (var document = WordprocessingDocument.Open(t.Filename, false))
            {
                var actualXml = new Parser(new MockLogger(), "Firmenname", "Firmensitz", XmlVariant.Bundesanzeiger, document).ConvertDocxToXml();
                actual = Util.GetStringFromXDocument(actualXml);
            }

            BundesanzeigerXmlCreator creator = TestXml.CreatorWithSampleDocument();
            creator.AddToRoot(creator.CreateXElement("A", "Hallo 1"));
            string expected = Util.GetStringFromXDocument(creator.XDocument);

            Assert.AreEqual(actual: actual, expected: expected);
        }

        [TestMethod]
        public void TestTableConversion1()
        {
            TestDoc t = TestDoc.GetMinimalTestDoc();

            t.AppendSimpleTable(Components.getSimpleTableString());

            string actual;
            using (var document = WordprocessingDocument.Open(t.Filename, false))
            {
                var actualXml = new Parser(new MockLogger(), "Firmenname", "Firmensitz", XmlVariant.Bundesanzeiger, document).ConvertDocxToXml();
                actual = Util.GetStringFromXDocument(actualXml);
            }

            BundesanzeigerXmlCreator creator = TestXml.CreatorWithSampleDocument();

            creator.AddToRoot(Components.GetSimpleXMLTable().ToNamespace(creator.XmlNamespace));
            string expected = Util.GetStringFromXDocument(creator.XDocument);

            Assert.AreEqual(actual: actual, expected: expected);
        }

        [TestMethod]
        public void TestTwoParagraphConversion()
        {
            TestDoc t = TestDoc.GetMinimalTestDoc();

            t.AppendParagraph("A", "Hallo 1");
            t.AppendParagraph("A", "Hallo 1");

            string actual;
            using (var document = WordprocessingDocument.Open(t.Filename, false))
            {
                var actualXml = new Parser(new MockLogger(), "Firmenname", "Firmensitz", XmlVariant.Bundesanzeiger, document).ConvertDocxToXml();
                actual = Util.GetStringFromXDocument(actualXml);
            }

            BundesanzeigerXmlCreator creator = TestXml.CreatorWithSampleDocument();
            creator.AddToRoot(creator.CreateXElement("A", "Hallo 1"));
            creator.AddToRoot(creator.CreateXElement("A", "Hallo 1"));
            string expected = Util.GetStringFromXDocument(creator.XDocument);

            Assert.AreEqual(actual: actual, expected: expected);
        }
    }
}
