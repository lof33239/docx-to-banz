﻿using System;
using WordZuBundesanzeigerCore;

namespace WordZuBundesanzeigerTest
{
    class MockLogger : ILogger
    {
        public void Log(string message) { }

        public void LogError(string message, Exception exception = null) { }

        public void LogWarning(string message) { }
    }
}
