﻿using System;
using Serilog;
using WordZuBundesanzeigerCore;
using WordZuBundesanzeigerCore.util;
using System.IO;
using Microsoft.Office.Interop.Word;
using DocumentFormat.OpenXml.Packaging;

namespace WordZuBundesanzeiger
{
    public partial class ThisAddIn
    {
        private BundesanzeigerRibbon ribbon1;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            Directory.CreateDirectory(Util.GetAppdataDirectory());
            File.AppendAllText(Util.GetLogFilePath(), $"[{DateTime.Now}]\tstarted\n");
            // Composition Root
            var serilogLogger = new LoggerConfiguration().WriteTo
                .File(Util.GetLogFilePath()).CreateLogger();
            var compositeLogger = new CompositeLogger(
                new DiagnosticsDebugLogger(),
                new SerilogLoggingAdapter(serilogLogger)
            );
            compositeLogger.Log("Logger active");
            try
            {
                var xmlMerger = new XmlMerger(compositeLogger);

                Parser createParser(string corporationName, string corporationLocation, XmlVariant variant, WordprocessingDocument document) =>
                    new Parser(compositeLogger, corporationName, corporationLocation, variant, document);

                ribbon1.CreateParser = createParser;
                ribbon1.xmlMerger = xmlMerger;
                ribbon1.Application = this.Application;
                ribbon1.logger = compositeLogger;
            }
            catch (Exception ex)
            {
                compositeLogger.LogError("Fataler Fehler", ex);
            }
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            File.AppendAllText(Util.GetLogFilePath(), $"[{DateTime.Now}]\tclosed\n");
        }

        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            this.ribbon1 = new BundesanzeigerRibbon();
            return Globals.Factory.GetRibbonFactory().CreateRibbonManager(
                new Microsoft.Office.Tools.Ribbon.IRibbonExtension[] {this.ribbon1});
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion
    }
}