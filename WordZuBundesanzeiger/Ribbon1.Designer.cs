﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.Office.Tools.Ribbon;

namespace WordZuBundesanzeiger
{
    partial class BundesanzeigerRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public string VersionLabel
        {
            get
            {
                if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
                {
                    Version ver = System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion;
                    return ver.ToString();
                }
                else
                {
                    var ver = Assembly.GetExecutingAssembly().GetName().Version;
                    return ver.ToString();
                }
            }
        }

        public BundesanzeigerRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;
            this.Bundesanzeiger.Label += " v" + VersionLabel; 
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Office.Tools.Ribbon.RibbonSeparator separator2;
            Microsoft.Office.Tools.Ribbon.RibbonDropDownItem ribbonDropDownItemImpl1 = this.Factory.CreateRibbonDropDownItem();
            Microsoft.Office.Tools.Ribbon.RibbonDropDownItem ribbonDropDownItemImpl2 = this.Factory.CreateRibbonDropDownItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BundesanzeigerRibbon));
            this.BundesanzeigerTab = this.Factory.CreateRibbonTab();
            this.Bundesanzeiger = this.Factory.CreateRibbonGroup();
            this.firmNameBox = this.Factory.CreateRibbonComboBox();
            this.firmLocationBox = this.Factory.CreateRibbonComboBox();
            this.uploadLocation = this.Factory.CreateRibbonDropDown();
            this.separator1 = this.Factory.CreateRibbonSeparator();
            this.saveXmlButton = this.Factory.CreateRibbonSplitButton();
            this.Preview_button = this.Factory.CreateRibbonButton();
            this.einstellungen = this.Factory.CreateRibbonMenu();
            this.zeigeWarnungen = this.Factory.CreateRibbonCheckBox();
            this.ueberschreibeWord = this.Factory.CreateRibbonCheckBox();
            this.xmlsHinzufuegen = this.Factory.CreateRibbonCheckBox();
            this.stylesButton = this.Factory.CreateRibbonButton();
            this.loescheFirmendaten = this.Factory.CreateRibbonButton();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.prependXmlsPicker = new System.Windows.Forms.OpenFileDialog();
            this.appendXmlsPicker = new System.Windows.Forms.OpenFileDialog();
            this.ShowAfterConvert = this.Factory.CreateRibbonCheckBox();
            separator2 = this.Factory.CreateRibbonSeparator();
            this.BundesanzeigerTab.SuspendLayout();
            this.Bundesanzeiger.SuspendLayout();
            this.SuspendLayout();
            // 
            // separator2
            // 
            separator2.Name = "separator2";
            // 
            // BundesanzeigerTab
            // 
            this.BundesanzeigerTab.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.BundesanzeigerTab.ControlId.OfficeId = "TabHome";
            this.BundesanzeigerTab.Groups.Add(this.Bundesanzeiger);
            this.BundesanzeigerTab.Label = "TabHome";
            this.BundesanzeigerTab.Name = "BundesanzeigerTab";
            // 
            // Bundesanzeiger
            // 
            this.Bundesanzeiger.Items.Add(this.firmNameBox);
            this.Bundesanzeiger.Items.Add(this.firmLocationBox);
            this.Bundesanzeiger.Items.Add(this.uploadLocation);
            this.Bundesanzeiger.Items.Add(this.separator1);
            this.Bundesanzeiger.Items.Add(this.saveXmlButton);
            this.Bundesanzeiger.Items.Add(this.einstellungen);
            this.Bundesanzeiger.Label = "Bundesanzeiger";
            this.Bundesanzeiger.Name = "Bundesanzeiger";
            // 
            // firmNameBox
            // 
            this.firmNameBox.Image = global::WordZuBundesanzeiger.Properties.Resources.industry;
            this.firmNameBox.Label = " Firma";
            this.firmNameBox.Name = "firmNameBox";
            this.firmNameBox.ScreenTip = "Geben Sie hier den Namen der Firma an, die das Dokument veröffentlicht.";
            this.firmNameBox.ShowImage = true;
            this.firmNameBox.ShowLabel = false;
            this.firmNameBox.SizeString = "Unternehmensregisteri";
            this.firmNameBox.Text = "Firmenname";
            this.firmNameBox.TextChanged += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ComboBox_TextChanged);
            // 
            // firmLocationBox
            // 
            this.firmLocationBox.Image = global::WordZuBundesanzeiger.Properties.Resources.map_marker_alt;
            this.firmLocationBox.Label = "Sitz";
            this.firmLocationBox.Name = "firmLocationBox";
            this.firmLocationBox.ScreenTip = "Geben Sie hier den Sitz der Firma an, die das Dokument veröffentlicht.";
            this.firmLocationBox.ShowImage = true;
            this.firmLocationBox.ShowItemImage = false;
            this.firmLocationBox.ShowLabel = false;
            this.firmLocationBox.SizeString = "Unternehmensregisteri";
            this.firmLocationBox.Text = "Firmensitz";
            this.firmLocationBox.TextChanged += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ComboBox_TextChanged);
            // 
            // uploadLocation
            // 
            this.uploadLocation.Image = global::WordZuBundesanzeiger.Properties.Resources.upload_alt;
            ribbonDropDownItemImpl1.Label = "Bundesanzeiger";
            ribbonDropDownItemImpl2.Label = "Unternehmensregister";
            this.uploadLocation.Items.Add(ribbonDropDownItemImpl1);
            this.uploadLocation.Items.Add(ribbonDropDownItemImpl2);
            this.uploadLocation.Label = "Art";
            this.uploadLocation.Name = "uploadLocation";
            this.uploadLocation.ScreenTip = "Geben Sie hier an, wo Sie das Dokument hochladen werden.";
            this.uploadLocation.ShowImage = true;
            this.uploadLocation.ShowItemImage = false;
            this.uploadLocation.ShowLabel = false;
            this.uploadLocation.SelectionChanged += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.UploadLocation_SelectionChanged);
            // 
            // separator1
            // 
            this.separator1.Name = "separator1";
            // 
            // saveXmlButton
            // 
            this.saveXmlButton.Image = ((System.Drawing.Image)(resources.GetObject("saveXmlButton.Image")));
            this.saveXmlButton.Items.Add(this.Preview_button);
            this.saveXmlButton.Label = "Erzeuge XML";
            this.saveXmlButton.Name = "saveXmlButton";
            this.saveXmlButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.SaveXmlButton_Click);
            // 
            // Preview_button
            // 
            this.Preview_button.Image = global::WordZuBundesanzeiger.Properties.Resources.eye;
            this.Preview_button.Label = "Vorschau";
            this.Preview_button.Name = "Preview_button";
            this.Preview_button.ShowImage = true;
            this.Preview_button.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.Preview_button_Click);
            // 
            // einstellungen
            // 
            this.einstellungen.Items.Add(this.zeigeWarnungen);
            this.einstellungen.Items.Add(this.ueberschreibeWord);
            this.einstellungen.Items.Add(this.xmlsHinzufuegen);
            this.einstellungen.Items.Add(this.ShowAfterConvert);
            this.einstellungen.Items.Add(separator2);
            this.einstellungen.Items.Add(this.stylesButton);
            this.einstellungen.Items.Add(this.loescheFirmendaten);
            this.einstellungen.Label = "Einstellungen";
            this.einstellungen.Name = "einstellungen";
            // 
            // zeigeWarnungen
            // 
            this.zeigeWarnungen.Checked = true;
            this.zeigeWarnungen.Label = "Zeige Warnungen";
            this.zeigeWarnungen.Name = "zeigeWarnungen";
            this.zeigeWarnungen.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ZeigeWarnungen_Click);
            // 
            // ueberschreibeWord
            // 
            this.ueberschreibeWord.Label = "Styles in Datei überschreiben";
            this.ueberschreibeWord.Name = "ueberschreibeWord";
            this.ueberschreibeWord.ScreenTip = "Wenn dieser Wert gesetzt ist, wird die aktuelle Word Datei mit den neuen Formatvo" +
    "rlagen überschrieben.";
            this.ueberschreibeWord.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.UeberschreibeWord_Click);
            // 
            // xmlsHinzufuegen
            // 
            this.xmlsHinzufuegen.Checked = true;
            this.xmlsHinzufuegen.Label = "Mit anderen XMLs kombinieren";
            this.xmlsHinzufuegen.Name = "xmlsHinzufuegen";
            this.xmlsHinzufuegen.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.BanzXmlVoranstellen_Click);
            // 
            // stylesButton
            // 
            this.stylesButton.Image = ((System.Drawing.Image)(resources.GetObject("stylesButton.Image")));
            this.stylesButton.Label = "Erzeuge Styles";
            this.stylesButton.Name = "stylesButton";
            this.stylesButton.ShowImage = true;
            this.stylesButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.StylesButton_Click);
            // 
            // loescheFirmendaten
            // 
            this.loescheFirmendaten.Image = global::WordZuBundesanzeiger.Properties.Resources.times;
            this.loescheFirmendaten.Label = "Lösche Firmendaten";
            this.loescheFirmendaten.Name = "loescheFirmendaten";
            this.loescheFirmendaten.ShowImage = true;
            this.loescheFirmendaten.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.LoescheFirmendaten_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "xml";
            this.saveFileDialog1.Filter = "XML Dateien|*.xml|Alle Dateien|*.*";
            this.saveFileDialog1.Title = "Bundesanzeiger XML speichern";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.SaveXmlDialog1_FileOk);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // prependXmlsPicker
            // 
            this.prependXmlsPicker.DefaultExt = "xml";
            this.prependXmlsPicker.Filter = "Xml-Dateien|*.xml|Alle Dateien|*.*";
            this.prependXmlsPicker.Multiselect = true;
            this.prependXmlsPicker.RestoreDirectory = true;
            this.prependXmlsPicker.SupportMultiDottedExtensions = true;
            this.prependXmlsPicker.Title = "Welche Dateien sollen VOR dem Dokument sein?";
            // 
            // appendXmlsPicker
            // 
            this.appendXmlsPicker.DefaultExt = "xml";
            this.appendXmlsPicker.Filter = "Xml-Dateien|*.xml|Alle Dateien|*.*";
            this.appendXmlsPicker.Multiselect = true;
            this.appendXmlsPicker.RestoreDirectory = true;
            this.appendXmlsPicker.SupportMultiDottedExtensions = true;
            this.appendXmlsPicker.Title = "Welche Dateien sollen HINTER dem Dokument sein?";
            // 
            // ShowAfterConvert
            // 
            this.ShowAfterConvert.Checked = true;
            this.ShowAfterConvert.Label = "Nach Konvertierung anzeigen";
            this.ShowAfterConvert.Name = "ShowAfterConvert";
            this.ShowAfterConvert.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ShowAfterConvert_Click);
            // 
            // BundesanzeigerRibbon
            // 
            this.Name = "BundesanzeigerRibbon";
            this.RibbonType = "Microsoft.Word.Document";
            this.Tabs.Add(this.BundesanzeigerTab);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon1_Load);
            this.BundesanzeigerTab.ResumeLayout(false);
            this.BundesanzeigerTab.PerformLayout();
            this.Bundesanzeiger.ResumeLayout(false);
            this.Bundesanzeiger.PerformLayout();
            this.ResumeLayout(false);

        }



        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab BundesanzeigerTab;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup Bundesanzeiger;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton stylesButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonSplitButton saveXmlButton;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        public Microsoft.Office.Tools.Ribbon.RibbonComboBox firmLocationBox;
        public Microsoft.Office.Tools.Ribbon.RibbonComboBox firmNameBox;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        internal RibbonButton Preview_button;
        internal RibbonCheckBox ueberschreibeWord;
        private System.Windows.Forms.ImageList imageList1;
        internal RibbonCheckBox xmlsHinzufuegen;
        internal RibbonSeparator separator1;
        private System.Windows.Forms.OpenFileDialog prependXmlsPicker;
        internal RibbonMenu einstellungen;
        internal RibbonCheckBox zeigeWarnungen;
        internal RibbonButton loescheFirmendaten;
        private System.Windows.Forms.OpenFileDialog appendXmlsPicker;
        internal RibbonDropDown uploadLocation;
        internal RibbonCheckBox ShowAfterConvert;
    }

    partial class ThisRibbonCollection
    {
        internal BundesanzeigerRibbon Ribbon1
        {
            get { return this.GetRibbon<BundesanzeigerRibbon>(); }
        }
    }
}
