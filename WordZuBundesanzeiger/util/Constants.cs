﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordZuBundesanzeiger.util
{
    class Constants
    {
        public const string CSS_RES = "Bundesanzeiger-Dateien";
        public static readonly Color WORD_BLUE = Color.FromArgb(43, 87, 154);
        public static readonly Color WORD_RED = Color.Red;

        public const string MOCK_TEXT = 
@"Hic eius nemo et animi saepe. Explicabo unde voluptates vel accusantium non. Dolor qui dolorem excepturi eveniet rerum reprehenderit et sit. Libero doloremque reprehenderit ipsum assumenda repellendus. Dolorem mollitia ut quos laborum ipsum. Ex ab et est quia quae.
Deserunt consequatur blanditiis facere. Et doloremque repellendus tenetur quod quia totam vero. Quos et facilis eveniet ab aut. Animi esse placeat dolor incidunt.
Et officia et expedita tempore molestiae labore repellendus ipsum. Voluptatum ratione illo maiores nam veniam natus. Officiis aut eum qui ipsa. Saepe nisi adipisci consectetur eaque. Voluptate quaerat doloribus voluptatem id.
Fugit eligendi ipsa suscipit voluptate. Ullam ad beatae aliquid ut voluptas. Possimus illum quisquam corporis nisi commodi aspernatur. Est consectetur est qui. Et maiores dolorem iste aperiam.
Eum magnam possimus earum illo debitis et quod. Illum accusamus harum necessitatibus repellat nihil voluptas. Repudiandae velit aspernatur voluptatem magni. Voluptas consequatur ab hic odit veritatis temporibus est blanditiis.
";
    }
}
