﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordZuBundesanzeiger.util
{
    class SettingsUtil
    {
        public static string FromDefaults(string key)
        {
            var defaults = Properties.Settings.Default.Defaults;
            foreach (var def in defaults)
            {
                var pair = def.Split(':');
                if (key.Equals(pair[0]))
                    return pair[1];
            }
            return null;
        }
    }
}
