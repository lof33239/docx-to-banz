﻿using System;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Text;
using System.Collections.Generic;
using System.Windows.Forms;

namespace WordZuBundesanzeiger.util
{
    
    class TextBoxUtil
    {
        // Taken from:
        // https://stackoverflow.com/questions/17586/best-word-wrap-algorithm/17635#17635
        //
        // Removed hyphen when splitting single long word.
        //
        static readonly char[] splitChars = new char[] { ' ', '-', '\t' };


        public static string WordWrap(string str, int width)
        {
            string[] words = Explode(str, splitChars);

            int curLineLength = 0;
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < words.Length; i += 1)
            {
                string word = words[i];
                // If adding the new word to the current line would be too long,
                // then put it on a new line (and split it up if it's too long).
                if (curLineLength + word.Length > width)
                {
                    // Only move down to a new line if we have text on the current line.
                    // Avoids situation where wrapped whitespace causes emptylines in text.
                    if (curLineLength > 0)
                    {
                        strBuilder.Append(Environment.NewLine);
                        curLineLength = 0;
                    }

                    // If the current word is too long to fit on a line even on it's own then
                    // split the word up.
                    while (word.Length > width)
                    {
                        strBuilder.Append(word.Substring(0, width));
                        word = word.Substring(width);

                        strBuilder.Append(Environment.NewLine);
                    }

                    // Remove leading whitespace from the word so the new line starts flush to the left.
                    word = word.TrimStart();
                }
                strBuilder.Append(word);
                curLineLength += word.Length;
            }

            return strBuilder.ToString();
        }

        private static string[] Explode(string str, char[] splitChars)
        {
            List<string> parts = new List<string>();
            int startIndex = 0;
            while (true)
            {
                int index = str.IndexOfAny(splitChars, startIndex);

                if (index == -1)
                {
                    parts.Add(str.Substring(startIndex));
                    return parts.ToArray();
                }

                string word = str.Substring(startIndex, index - startIndex);
                char nextChar = str.Substring(index, 1)[0];
                // Dashes and the likes should stick to the word occuring before it. Whitespace doesn't have to.
                if (char.IsWhiteSpace(nextChar))
                {
                    parts.Add(word);
                    parts.Add(nextChar.ToString());
                }
                else
                {
                    parts.Add(word + nextChar);
                }

                startIndex = index + 1;
            }
        }

        public static bool CheckForScrollbars(TextBox textBox, int lines)
        {
            bool scroll = false;
            if (lines > 1)
            {
                scroll = GetTextContentHeight(textBox, lines) > (textBox.ClientSize.Height);
            }
            return scroll;
        }

        public static int GetTextContentHeight(TextBox textBox, int lines)
        {
            if (lines > 1)
            {
                int h = textBox.Font.Height;
                return lines * h + 6;
            }
            else
            {
                return textBox.Font.Height;
            }
        }

    }
}
