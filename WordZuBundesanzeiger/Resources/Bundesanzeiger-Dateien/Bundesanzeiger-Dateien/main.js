var t31_piwik = {
		category: "Teaser Schnellzugriff",
		action: "Klick - Teaser Schnellzugriff",
		param1: "zum Amtlichen Teil",
		param2: "zu den Fondspreisen",
		param3: "zu den Netto-Leerverkaufspositionen",
		param4: "zum Aktionärsforum"
}
var t32_piwik = {
		category: "Teaser Verlag",
		action: "Klick - Teaser Verlag",
		param1: "Unternehmensregister",
		param2: "Bundesgesetzblatt",
		param3: "eBilanz-Online",
		param4: "Bundesanzeiger-Fachmedien",
		param5: "Datenservice",
		param6: "Unternehmen und Wirtschaft",
		param7: "Verlagshomepage",
		param8: "European Business Register",
                param9: "Legal Entity Identifier Register",
                param10: "Transparenzregister"
}

function init_fonddata()
{
	var field = $('#fonddata_search_param\\.stext');
	var str = search_field_placeholder;
	field.focus(function()
	{
		var input = $(this);
		if (input.val() == str)
		{
			input.val('');
		}
	});
	/*
	field.focusout(function()
	{
		var input = $(this);
		if ($.trim(input.val()) == '')
		{
			input.val(str);
		}
	});
	*/
}

function init_fonddata_resultlist()
{
	var fields = $('.fonddata_resultlist_type_switch input.radio');
	var button = $('.fonddata_resultlist_type_switch input.button');
	button.hide();
	fields.change(function()
	{
		button.click();
	});
}

$(function()
{
    // hide browser hint

    $('#browser_hint .close').attr('href', '#').click(function(event)
    {
        $(this).parent().slideUp();
        document.cookie = 'hide_browser_hint=true;path=/';
    });

    // shorten long search result entries

    var lang = $('#flags li.active a').attr('class'),
        max_length = 500,
        less_text = (lang == 'en' ? 'less' : 'weniger'),
        less_title = (lang == 'en' ? 'Show first names' : 'Erste Namen anzeigen'),
        more_text = (lang == 'en' ? 'more' : 'mehr'),
        more_title = (lang == 'en' ? 'Show all names' : 'Alle Namen anzeigen');

    $('table.result td.first').each(function()
    {
        var text = $(this).text();
        if (text.length > max_length)
        {
            $(this).html(
                '<div style="display:none;">' + text + '</div>' +
                '<div>' + text.substr(0, max_length) + '&nbsp;&hellip;</div>' +
                '<a href="#" class="intern" title="' + less_title + '" style="display:none;">' + less_text + '</a>' +
                '<a href="#" class="intern" title="' + more_title + '">' + more_text + '</a>'
            );
        }
    });

    $('td.first a').click(function()
    {
        $(this).parent().children().slideToggle();
    });
});
