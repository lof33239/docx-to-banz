﻿using DocumentFormat.OpenXml.Packaging;
using Microsoft.Office.Interop.Word;
using Microsoft.Office.Tools.Ribbon;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml.Schema;
using WordZuBundesanzeigerCore;
using WordZuBundesanzeigerCore.util;
using System.Text.RegularExpressions;

namespace WordZuBundesanzeiger
{
    public sealed partial class BundesanzeigerRibbon : IWarningListener
    {


        public Microsoft.Office.Interop.Word.Application Application { get; set; }
        public Func<string, string, XmlVariant, WordprocessingDocument, Parser> CreateParser { get; set; }
        public ILogger logger;

        private readonly Dictionary<string, bool> Entered = new Dictionary<string, bool>();
        internal XmlMerger xmlMerger;

        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {
            LoadSettings();

            Entered[this.firmNameBox.Name] = false;
            Entered[this.firmLocationBox.Name] = false;

        }

        private void LoadSettings()
        {
            LoadListItems(this.firmNameBox);
            LoadListItems(this.firmLocationBox);

            this.firmNameBox.Text = util.SettingsUtil.FromDefaults(this.firmNameBox.Name);
            this.firmLocationBox.Text = util.SettingsUtil.FromDefaults(this.firmLocationBox.Name);

            ShowAfterConvert.Checked = (bool)Properties.Settings.Default.ShowAfterConvert;
            zeigeWarnungen.Checked = (bool)Properties.Settings.Default.zeigeWarnungen;
            ueberschreibeWord.Checked = (bool)Properties.Settings.Default.ueberschreibeWord;
            xmlsHinzufuegen.Checked = (bool)Properties.Settings.Default.bilanzAnhaengen;
            uploadLocation.SelectedItemIndex = (int)Properties.Settings.Default.uploadLocation;
        }

        private void LoadListItems(RibbonComboBox comboBox)
        {
            comboBox.Items.Clear();

            if (!(Properties.Settings.Default[comboBox.Name] is List<string> labelList))
                return;
            foreach (var label in labelList)
            {
                var item = this.Factory.CreateRibbonDropDownItem();
                item.Label = label;
                comboBox.Items.Add(item);
            }

        }

        private void SaveListItems(RibbonComboBox comboBox)
        {
            if (!(Properties.Settings.Default[comboBox.Name] is List<string> labelList))
                labelList = new List<string>();

            labelList.Add(comboBox.Text);

            Properties.Settings.Default[comboBox.Name] = labelList;
            Properties.Settings.Default.Save();
            LoadListItems(comboBox);
        }

        private void ResetListItems(RibbonComboBox comboBox)
        {
            Properties.Settings.Default[comboBox.Name] = new List<string>();
            Properties.Settings.Default.Save();
            LoadListItems(comboBox);
        }
        private void ComboBox_TextChanged(object sender, RibbonControlEventArgs e)
        {
            var box = sender as RibbonComboBox;
            if ("" == box.Text)
            {
                Entered[box.Name] = false;
                box.Text = util.SettingsUtil.FromDefaults(box.Name);
            }
            else
            {
                if (box.Name == firmNameBox.Name)
                    box.Image = global::WordZuBundesanzeiger.Properties.Resources.industry;
                else if (box.Name == firmLocationBox.Name)
                    box.Image = global::WordZuBundesanzeiger.Properties.Resources.map_marker_alt;
                Entered[box.Name] = true;
            }
        }

        private void StylesButton_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                if (this.ueberschreibeWord.Checked)
                    Application.ActiveDocument.Save();
                else
                {
                    const int OK = -1;
                    var status = Application.Dialogs[WdWordDialog.wdDialogFileSaveAs].Show();
                    if (status != OK) throw new COMException("Speicher abgebrochen");
                }
            }
            catch (COMException)
            {
                const string Text = "Formatvorlagen wurden nicht erzeugt!\n\n" +
                    "Das Dokument muss gespeichert werden," +
                    " damit die Formatvorlagen erzeugt werden können.";

                MessageBox.Show(Text, "Warnung", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            var activeDocumentPath = Application.ActiveDocument.FullName;
            activeDocumentPath = HandleOneDrive(activeDocumentPath);
            Application.ActiveDocument.Close();
            using (var wpDocument = WordprocessingDocument.Open(activeDocumentPath, true))
            {
                new TemplateGen().InsertParts(wpDocument);
            }
            this.Application.Documents.Open(activeDocumentPath);
        }

        private string HandleOneDrive(string activeDocumentPath)
        {
            if (!activeDocumentPath.StartsWith("http"))
                return activeDocumentPath;

            var oneDrivePath = Environment.GetEnvironmentVariable("OneDriveConsumer");

            //Vielleicht irgendwann mal schöner machen
            int cursor;
            cursor = activeDocumentPath.IndexOf("//") + 2;
            cursor = activeDocumentPath.IndexOf("/", cursor) + 1;
            cursor = activeDocumentPath.IndexOf("/", cursor) + 1;
            activeDocumentPath = Path.Combine(oneDrivePath, activeDocumentPath.Substring(cursor));
            return Regex.Replace(activeDocumentPath, @"\\\\|/", Application.PathSeparator);
        }

        private void SaveXmlButton_Click(object sender, RibbonControlEventArgs e)
        {
            var xml = GetXDocumentFromWord();
            if (null == xml) { return; }
            SaveListItems(this.firmNameBox);
            SaveListItems(this.firmLocationBox);
            SaveBundesanzeigerXml(xml);
            if (ShowAfterConvert.Checked)
            {
                ShowHtml(xml);
            }
        }

        private XDocument GetXDocumentFromWord()
        {
            XDocument xDocument;
            if (!IsRibbonDataValid())
            {
                ShowProblemIcons();
                return null;
            }

            var wordTempFileName = Util.GetTempfileName();
            var wordTempXmlFileName = $"{wordTempFileName}.xml";
            var wordTempDocxFileName = $"{wordTempFileName}.";
            WordprocessingDocument document = null;

            try
            {
                ConvertWordXmlStringToDocx(wordTempXmlFileName, wordTempDocxFileName);

                document = WordprocessingDocument.Open(wordTempDocxFileName, false);
                var variant = Util.GetXmlVariantByLabel(uploadLocation.SelectedItem.Label);
                var parser = CreateParser(firmNameBox.Text, firmLocationBox.Text, variant, document);
                parser.SubscribeParserWarning(this);

                xDocument = parser.ConvertDocxToXmlWithValidation();
            }
            catch (WordParseException exception)
            {
                if (exception.ParagraphCaret > 0)
                {
                    Application.Selection.SetRange(exception.ParagraphCaret, exception.ParagraphCaret);
                    var relevantParagraph = Application.Selection.Paragraphs.First;
                    //relevantParagraph.Range.HighlightColorIndex = WdColorIndex.wdRed;

                    Application.ActiveWindow.ScrollIntoView(relevantParagraph.Range);
                    Application.ActiveDocument.Comments.Add(relevantParagraph.Range, exception.Message);
                }
                MessageBox.Show(exception.Message, "Fehler bei der Konvertierung", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            }
            catch (XmlSchemaValidationException exception)
            {
                if (exception.Message.Contains("unvollständig"))
                {
                    MessageBox.Show(
                        "Im Dokument fehlt ein Paragraph mit der Formatvorlage '__Titel__', "
                        + "ohne diesen Paragraph kann keine gültige Datei erzeugt werden.",
                        "Datei konnte nicht erzeugt werden",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
                else if (exception.Message.Contains("Das Element 'Bekanntmachungskopf' in Namespace 'http://www.ebundesanzeiger.de/publikation/layout' hat ein ungültiges untergeordnetes Element 'Titel' in Namespace 'http://www.ebundesanzeiger.de/publikation/layout'."))
                {
                    MessageBox.Show(
                        "Das Dokument enthält mehr als einen Paragraphen mit der Formatvorlage '__Titel__'.\n\n "
                        + "Wenn der Titel mehr als eine Zeile überspannen soll, verwenden Sie bitte Shift+Enter "
                        + "(statt nur Enter). So wird statt eines neuen Paragraphen eine neue Zeile begonnen.\n "
                        + "In einer zukünftigen Version soll dies automatisch geschehen.",
                        "Datei konnte nicht erzeugt werden",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show(
                        exception.Message,
                        "Datei konnte nicht erzeugt werden - Unerwarteter Fehler",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }

                return null;
            }
            catch (Exception e)
            {
                MessageBox.Show(
                        e.Message,
                        "Unerwarteter Fehler",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                logger.LogError("Unerwarteter Fehler", e);
                return null;
            }
            finally
            {
                document?.Dispose();

                if (File.Exists(wordTempXmlFileName))
                    File.Delete(wordTempXmlFileName);

                if (File.Exists(wordTempDocxFileName))
                    File.Delete(wordTempDocxFileName);
            }
            if (xmlsHinzufuegen.Checked) xDocument = AddXmlsTo(xDocument);
            return xDocument;
        }

        private DialogResult ShowDialog(FileDialog dialog)
        {
            NativeWindow main = new NativeWindow();
            main.AssignHandle(new IntPtr(Application.ActiveWindow.Hwnd));
            var result = dialog.ShowDialog(main);
            main.ReleaseHandle();
            return result;
        }

        private XDocument AddXmlsTo(XDocument xDocument)
        {

            var result = ShowDialog(this.prependXmlsPicker);
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                xDocument = this.xmlMerger.Prepend(this.prependXmlsPicker.FileNames, xDocument);
            }
            result = ShowDialog(this.appendXmlsPicker);
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                xDocument = this.xmlMerger.Append(this.appendXmlsPicker.FileNames, xDocument);
            }
            return xDocument;
        }

        private void SaveBundesanzeigerXml(XDocument xdoc)
        {
            saveFileDialog1.InitialDirectory = Application.ActiveDocument.Path;
            saveFileDialog1.FileName = Path.GetFileNameWithoutExtension(Application.ActiveDocument.Name);
            var result = this.saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                xdoc.Save(saveFileDialog1.FileName);
                MessageBox.Show($"Die Datei wurde unter {saveFileDialog1.FileName} gespeichert", "Erfolg",
                    MessageBoxButtons.OK, MessageBoxIcon.None);
            }
        }

        private void ConvertWordXmlStringToDocx(string xmlFileName, string docxFileName)
        {
            System.IO.File.WriteAllText(xmlFileName, this.Application.ActiveDocument.WordOpenXML);

            var temp_doc = this.Application.Documents.Open(xmlFileName, ConfirmConversions: false, ReadOnly: true, Visible: false);
            temp_doc.SaveAs2(docxFileName, FileFormat: WdSaveFormat.wdFormatDocumentDefault);
            temp_doc.Close();
        }


        private void ShowProblemIcons()
        {
            if (!Entered[firmNameBox.Name])
                firmNameBox.Image = global::WordZuBundesanzeiger.Properties.Resources.times;
            if (!Entered[firmLocationBox.Name])
                firmLocationBox.Image = global::WordZuBundesanzeiger.Properties.Resources.times;
            MessageBox.Show(
                "Sie haben keinen Firmennamen bzw. -ort angegeben.\nBitte prüfen Sie Ihre Eingabe in der oberen Leiste.",
                "Firmenname/Firmenort fehlt", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private bool IsRibbonDataValid()
        {
            return Entered[firmNameBox.Name] && Entered[firmLocationBox.Name];
        }

        private void SaveXmlDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void Preview_button_Click(object sender, RibbonControlEventArgs e)
        {
            var xml = GetXDocumentFromWord();
            if (null != xml)
                ShowHtml(xml);
        }

        private void ShowHtml(XDocument xDocument)
        {
            var appDocumentDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "WordZuBundesanzeiger");
            Directory.CreateDirectory(appDocumentDirectory);
            var htmlPath = Path.Combine(appDocumentDirectory, "temp_html.html");
            var cssPath = Path.Combine(appDocumentDirectory, util.Constants.CSS_RES);
            if (!Directory.Exists(cssPath))
            {
                MessageBox.Show($"Um die Vorschau korrekt darzustellen wird der folgende Ordner benötigt:\n\n{cssPath}\n\n Er scheint nicht vorhanden zu sein.\n\n Vorgang wird abgebrochen.", "Css Dateien fehlen", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            var html = XmlToHtml.GetHtml(xDocument);
            html.Save(htmlPath);
            Process.Start(htmlPath);

        }

        public void OnParserWarning(string message)
        {
            if (zeigeWarnungen.Checked)
                MessageBox.Show(message, "Achtung", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void LoescheFirmendaten_Click(object sender, RibbonControlEventArgs e)
        {
            ResetListItems(firmNameBox);
            ResetListItems(firmLocationBox);
        }

        private void ZeigeWarnungen_Click(object sender, RibbonControlEventArgs e)
        {
            Properties.Settings.Default.zeigeWarnungen = zeigeWarnungen.Checked;
            Properties.Settings.Default.Save();
        }

        private void BanzXmlVoranstellen_Click(object sender, RibbonControlEventArgs e)
        {
            Properties.Settings.Default.bilanzAnhaengen = xmlsHinzufuegen.Checked;
            Properties.Settings.Default.Save();
        }

        private void UeberschreibeWord_Click(object sender, RibbonControlEventArgs e)
        {
            Properties.Settings.Default.ueberschreibeWord = ueberschreibeWord.Checked;
            Properties.Settings.Default.Save();
        }


        private void UploadLocation_SelectionChanged(object sender, RibbonControlEventArgs e)
        {
            Properties.Settings.Default.uploadLocation = Util.GetXmlVariantByLabel(uploadLocation.SelectedItem.Label);
            Properties.Settings.Default.Save();
        }

        private void ShowAfterConvert_Click(object sender, RibbonControlEventArgs e)
        {
            Properties.Settings.Default.ShowAfterConvert = ShowAfterConvert.Checked;
            Properties.Settings.Default.Save();
        }
    }
}
