﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WordZuBundesanzeigerCore
{
    public interface IWarningListener
    {
        void OnParserWarning(string message);
    }
}
