﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace WordZuBundesanzeigerCore
{
    public sealed class Parser
    {
        private readonly ICollection<IWarningListener> parserWarningListeners = new HashSet<IWarningListener>();
        private readonly ILogger log;

        
        private readonly WordprocessingDocument wordDocument;
        private readonly FootnoteParser footnoteParser;
        private readonly BundesanzeigerXmlCreator xmlCreator;

        private XElement currentList;
        private int xmlIndentationLevel = -1;
        private bool nextUnterzeichnerAlreadyHandled = false;

        public Parser(ILogger log, string corporationName, string corporationLocation, XmlVariant variant, WordprocessingDocument wordDocument)
        {
            this.log = log ?? throw new ArgumentNullException(nameof(log));
            this.wordDocument = wordDocument;
            xmlCreator = new BundesanzeigerXmlCreator(corporationName, corporationLocation, variant);
            footnoteParser = new FootnoteParser(wordDocument, log, xmlCreator);
        }

        public XDocument ConvertDocxToXml()
        {
            var mainDocumentPart = wordDocument.MainDocumentPart;
            var wordDocumentBody = mainDocumentPart.Document.Body;

            if (wordDocumentBody.Descendants<Run>().Any(r => r.RunProperties?.Underline != null))
                ParserWarningEvent("Unterstreichungen können nicht in Bundesanzeiger XML übernommen werden. Wenn eine Hervorhebung wichtig ist, können Sie Fettschrift oder Kursivschrift verwenden");
            if (wordDocumentBody.Descendants<EmbeddedObject>().Any(_ => true))
                throw new WordParseException("Das Dokument enthält ein eingebettetes Element. Diese können bei der Konvertierung nicht verarbeitet werden und müssen aus dem Dokument entfernt werden.");

            foreach (var node in wordDocumentBody.ChildElements)
            {
                if (node is Paragraph paragraph)
                    HandleParagraph(paragraph);
                else if (node is Table table) HandleTable(table);
            }

            if (footnoteParser.HasFootnotes)
            {
                xmlCreator.AddToRoot(footnoteParser.FootNoteBereich);
            }

            return xmlCreator.XDocument;
        }

        public XDocument ConvertDocxToXmlWithValidation()
        {
            ConvertDocxToXml();
            xmlCreator.Validate();

            return xmlCreator.XDocument;
        }


        private string GetParagraphType(Paragraph paragraph)
        {
            var styleId = paragraph?.ParagraphProperties?.ParagraphStyleId?.Val?.Value;
            if (null == styleId)
            {
                var caret = paragraph.ElementsBefore().Sum(x => x.InnerText.Length);
                throw new WordParseException($"Der Paragraph bei Cursorposition {caret} besitzt keine Formatvorlage. Bitte wählen Sie eine geeignete aus.",
                    caret);
            }
            var styleWithId = wordDocument.MainDocumentPart.StyleDefinitionsPart.Styles.Elements<Style>()
                .First(x => x.StyleId.Value.Equals(styleId));
            var styleName = styleWithId.Elements<StyleName>().Single().Val.Value;
            if (BanzXml.parNameMap.Reverse.ContainsKey(styleName))
                return BanzXml.parNameMap.Reverse[styleName];
            else
            {
                var caret = paragraph.ElementsBefore().Sum(x => x.InnerText.Length);
                throw new WordParseException($"Der Paragraph bei Cursorposition {caret} besitzt die unbekannte Formatvorlage '{styleName}'. Bitte wählen Sie eine geeignete aus.",
                    caret);
            }

        }

        private void HandleParagraph(Paragraph paragraph)
        {
            var type = GetParagraphType(paragraph);

            if (type == BanzXml.DontConvert) return;
            if (type == BanzXml.Unterzeichner && nextUnterzeichnerAlreadyHandled) return;
            else nextUnterzeichnerAlreadyHandled = false;

            if (type == BanzXml.Unterzeichner)
            {
                HandleSigners(paragraph);
            }
            else if (type == BanzXml.Titel)
            {
                HandleTitle(paragraph);
            }
            else if (paragraph.ParagraphProperties?.NumberingProperties?.NumberingId?.Val?.Value != null)
            {
                var numid = paragraph.ParagraphProperties.NumberingProperties.NumberingId.Val.Value;
                var numbering = this.wordDocument.MainDocumentPart.NumberingDefinitionsPart.Numbering;
                var absNumId = numbering.Elements<NumberingInstance>().First(x => x.NumberID == numid).AbstractNumId;
                var ordered = numbering
                    .Elements<AbstractNum>()
                    .First(x => x.AbstractNumberId == absNumId.Val.Value)
                    .Elements<Level>()
                    .Any(x => x.LevelText.Val.Value.Contains("%"));
                if (ordered)
                    type = BanzXml.OL;
                else
                    type = BanzXml.UL;
                HandleLists(paragraph, type);
            }
            else
            {
                HandleSimpleParagraph(paragraph, type);
                ResetIndentation();
            }
        }

        private void HandleSimpleParagraph(Paragraph paragraph, string type)
        {
            var simpleNode = xmlCreator.CreateXElement(type);
            ExtractParagraphContent(paragraph, simpleNode, type);
            xmlCreator.AddToRoot(simpleNode);
        }

        private void ResetIndentation()
        {
            if (xmlIndentationLevel >= 0)
            {
                xmlIndentationLevel = -1;
                currentList = null;
            }
        }

        private void HandleLists(Paragraph paragraph, string listType)
        {
            if (currentList != null && !currentList.Name.LocalName.Equals(listType))
                ResetIndentation();

            if (xmlIndentationLevel < 0)
            {

                currentList = xmlCreator.CreateXElement(listType);
                xmlCreator.AddToRoot(currentList);
                xmlIndentationLevel = 0;
            }
            int wordIndentationLevel;
            if (paragraph.ParagraphProperties?.NumberingProperties?.NumberingLevelReference?.Val?.Value != null)
                wordIndentationLevel = paragraph.ParagraphProperties.NumberingProperties.NumberingLevelReference.Val.Value;
            else
                wordIndentationLevel = 0;
            if (wordIndentationLevel > xmlIndentationLevel)
            {
                for (var j = xmlIndentationLevel; j < wordIndentationLevel; j++)
                {

                    var newList = xmlCreator.CreateXElement(listType);
                    currentList.Add(xmlCreator.CreateXElement(BanzXml.LI,
                                                                            xmlCreator.CreateXElement(BanzXml.LA, newList)));
                    currentList = newList;
                }

                xmlIndentationLevel = wordIndentationLevel;
            }
            else if (wordIndentationLevel < xmlIndentationLevel)
            {
                for (var j = wordIndentationLevel; j < xmlIndentationLevel; j++)
                    currentList = currentList.Parent.Parent.Parent;

                xmlIndentationLevel = wordIndentationLevel;
            }

            var listParagraphNode = xmlCreator.CreateXElement(BanzXml.LA);
            ExtractParagraphContent(paragraph, listParagraphNode, listType);
            var listElement = xmlCreator.CreateXElement(BanzXml.LI, listParagraphNode);
            currentList.Add(listElement);
            return;
        }

        private void HandleTitle(Paragraph paragraph)
        {
            var titleNode = xmlCreator.CreateXElement(BanzXml.Titel);
            ExtractParagraphContent(paragraph, titleNode, BanzXml.Titel);
            xmlCreator.AddToHead(titleNode);
        }

        private void HandleSigners(Paragraph paragraph)
        {
            paragraph = MergeFollowingSignerParagraphs(paragraph);
            (var table, var row) = AddSignerTable();

            foreach (var par in GetSignerParagraphs(paragraph))
            {
                HandleSigner(par, table, row);
            }
            return;
        }

        private (XElement, XElement) AddSignerTable()
        {
            var row = xmlCreator.CreateXElement(BanzXml.TR);
            var tbody = xmlCreator.CreateXElement(BanzXml.TBODY, row);
            var table = xmlCreator.CreateXElement(BanzXml.TABLE, tbody);
            xmlCreator.AddToRoot(table);
            return (table, row);
        }

        private Paragraph MergeFollowingSignerParagraphs(Paragraph paragraph)
        {
            var following = paragraph.ElementsAfter().TakeWhile(el => el is Paragraph p && GetParagraphType(p) == BanzXml.Unterzeichner);
            var ret = (Paragraph)paragraph.Clone();
            foreach (var par in following)
            {
                var cloned = par.Elements<Run>().Select(el => (Run)el.Clone()).ToArray();
                if (cloned.Length == 0) continue;
                cloned[0].InsertAt(new Break(), 0);
                ret.Append(cloned);
            }
            nextUnterzeichnerAlreadyHandled = true;
            return ret;
        }

        private IEnumerable<Paragraph> GetSignerParagraphs(Paragraph paragraph)
        {
            var rows = GetParagraphRows(paragraph).ToList();
            while (rows.Select(row => row.Count()).Max() > 0)
            {
                var curPar = CloneParaphSignature(paragraph);
                rows = rows.Select(row =>
                {
                    var name = row.TakeWhile(e => !(e is TabChar));
                    var runContent = name.Prepend(new Break());
                    curPar.Append(new Run(runContent));
                    row = row.Skip(name.Count());
                    row = row.SkipWhile(element => (element is TabChar) || (element is RunProperties));
                    return row;
                }).ToList();
                curPar.Descendants<Break>().First().Remove();
                yield return curPar;
            }
        }

        private Paragraph CloneParaphSignature(Paragraph paragraph)
        {
            var curPar = paragraph.Clone() as Paragraph;
            var prop = curPar.ParagraphProperties;
            curPar.RemoveAllChildren();
            curPar.Append(prop);
            return curPar;
        }

        private IEnumerable<IEnumerable<OpenXmlElement>> GetParagraphRows(Paragraph paragraph)
        {
            var elements = paragraph.Elements<Run>().SelectMany(run => run.Elements()).Select(element => element.CloneNode(true));
            while (elements.Any())
            {
                yield return elements.TakeWhile(x => !(x is Break));
                elements = elements.SkipWhile(x => !(x is Break));
                elements = elements.Skip(1);
            }
        }

        private void HandleSigner(Paragraph paragraph, XElement table, XElement row)
        {
            if ("".Equals(paragraph.InnerText))
                return;

            var signerNode = xmlCreator.CreateXElement(BanzXml.Unterzeichner);
            ExtractParagraphContent(paragraph, signerNode, BanzXml.Unterzeichner);
            var signatureNode = xmlCreator.CreateXElement(BanzXml.Signatur, signerNode);
            var justAttr = new XAttribute(BanzXml.ATTR_ALIGN, BanzXml.ALIGN_LEFT);
            var td = xmlCreator.CreateXElement(BanzXml.TD, signatureNode, justAttr);

            table.AddFirst(xmlCreator.CreateXElement(BanzXml.COL));
            row.Add(td);
            return;
        }

        private void ExtractParagraphContent(Paragraph paragraph, XElement currentXmlNode, string paragraphType)
        {
            var baseNode = currentXmlNode;
            foreach (var node in paragraph.ChildElements)
            {
                currentXmlNode = baseNode;
                if (!(node is Run run))
                    continue;

                if (run.RunProperties != null &&
                    (paragraphType == BanzXml.A ||
                     paragraphType == BanzXml.LA ||
                     paragraphType == BanzXml.TD))
                {
                    var isBold = run.RunProperties.Bold != null;
                    var isItalic = run.RunProperties.Italic != null;
                    var nodeName = isBold && isItalic ? BanzXml.BoldItalic :
                                   isBold ? BanzXml.Bold :
                                   isItalic ? BanzXml.Italic : null;
                    if (nodeName != null)
                    {
                        var newXmlNode = xmlCreator.CreateXElement(nodeName);
                        currentXmlNode.Add(newXmlNode);
                        currentXmlNode = newXmlNode;
                    }
                }

                foreach (var elem in run.ChildElements)
                {
                    if (elem is Text text)
                        currentXmlNode.Add(text.InnerText);
                    else if (elem is Break)
                        currentXmlNode.Add(xmlCreator.CreateXElement(BanzXml.nZ));
                    else if (elem is FootnoteReference refer)
                    {
                        footnoteParser.addFootnoteRef(refer, currentXmlNode);
                    }
                }
            }
        }

        private void HandleTable(Table docTable)
        {
            var table = xmlCreator.CreateXElement(BanzXml.TABLE);
            XElement curParent = null;

            var inTh = false;
            var inTd = false;

            var tableGrid = docTable.Elements<TableGrid>().First();

            var columns = tableGrid.Elements<GridColumn>();
            var tableWidth = columns.Sum(col => Convert.ToInt32(col.Width?.Value ?? "1"));

            foreach (var col in columns)
            {
                var colWidth = Convert.ToInt32(col.Width?.Value ?? "1");
                var percentage = (colWidth * 100) / tableWidth;
                if (percentage <= 0) percentage = 100 / columns.Count();
                var widthAttr = new XAttribute(BanzXml.ATTR_WIDTH, $"{percentage}%");
                table.Add(xmlCreator.CreateXElement(BanzXml.COL, widthAttr));
            }




            foreach (var tablerow in docTable.Descendants<TableRow>())
            {
                var type = GetParagraphType(tablerow.Descendants<Paragraph>().First());
                switch (type)
                {
                    case BanzXml.DontConvert:
                        throw new WordParseException("In Tabellen darf momentan kein Inhalt stehen, der nicht kovertiert werden soll. " +
                            "Ist diese Funktionalität gefordert, wenden Sie sich bitte an die Entwickler.");
#pragma warning disable CS0162 // Unreachable code detected
                        break;
#pragma warning restore CS0162 // Unreachable code detected
                    case BanzXml.THEAD:
                        if (inTd)
                        {
                            var before = tablerow.ElementsBefore().Concat(docTable.ElementsBefore());
                            var caret = before.Sum(x => x.InnerText.Length) + tablerow.ElementsBefore().Count(x => true) * 2 + docTable.ElementsBefore().Count(x => true) - 3;
                            throw new WordParseException($"Nach dem Tabellenkörper darf in einer Tabelle kein Tabellenkopf liegen. Siehe Cursorposition {caret}.",
                                caret);
                        }
                        if (!inTh)
                        {
                            inTh = true;
                            curParent = xmlCreator.CreateXElement(BanzXml.THEAD);
                            table.Add(curParent);
                        }

                        break;
                    case BanzXml.TBODY:
                        if (!inTd)
                        {
                            inTd = true;
                            curParent = xmlCreator.CreateXElement(BanzXml.TBODY);
                            table.Add(curParent);
                        }

                        break;
                    default:
                        log.LogWarning($"Wrong style id in table. Must be {BanzXml.THEAD} or {BanzXml.TBODY}. Assuming {BanzXml.TBODY}.");
                        if (!inTd)
                        {
                            inTd = true;
                            curParent = xmlCreator.CreateXElement(BanzXml.TBODY);
                            table.Add(curParent);
                        }

                        break;
                }

                XElement xtableRow = xmlCreator.CreateXElement(BanzXml.TR);
                foreach (var tableCell in tablerow.Descendants<TableCell>())
                {
                    var colspan = tableCell?.TableCellProperties?.GridSpan?.Val ?? 1;
                    var colspanAttr = new XAttribute(BanzXml.ATTR_COLSPAN, colspan);
                    var justification = tableCell.Descendants<Paragraph>().First().ParagraphProperties?.Justification?.Val ?? JustificationValues.Left;
                    var justAttr = new XAttribute(BanzXml.ATTR_ALIGN, BanzXml.JustMap(justification));

                    var td = xmlCreator.CreateXElement(BanzXml.TD, colspanAttr, justAttr);

                    var j = 0;
                    var descendants = tableCell.Descendants<Paragraph>();
                    var dlength = descendants.LongCount();
                    foreach (var node in descendants)
                    {
                        j++;
                        ExtractParagraphContent(node, td, BanzXml.TD);
                        if (j < dlength)
                        {
                            td.Add(xmlCreator.CreateXElement(BanzXml.nZ));
                        }
                    }
                    xtableRow.Add(td);
                }

                curParent.Add(xtableRow);
            }

            if (!inTd) throw new WordParseException("There is a table that does not contain a tablebody.");
            xmlCreator.AddToRoot(table);
        }

        private void ParserWarningEvent(string message)
        {
            foreach (var listener in parserWarningListeners)
            {
                listener.OnParserWarning(message);
            }
        }

        public void SubscribeParserWarning(IWarningListener listener)
        {
            parserWarningListeners.Add(listener);
        }

        public void UnsubscribeParserWarning(IWarningListener listener)
        {
            parserWarningListeners.Remove(listener);
        }
    }
}