﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

namespace WordZuBundesanzeigerCore
{
    public class TemplateGen
    {
        private Styles styles;

        private readonly JustificationValues left = JustificationValues.Left;
        private readonly JustificationValues both = JustificationValues.Both;
        private readonly JustificationValues center = JustificationValues.Center;

        public TemplateGen()
        {
        }

        public void InsertParts(WordprocessingDocument doc)
        {
            doc.MainDocumentPart.DeletePart(doc.MainDocumentPart.StyleDefinitionsPart);
            doc.MainDocumentPart.DeletePart(doc.MainDocumentPart.NumberingDefinitionsPart);
            Initialize(doc);
            AddStyles(doc);
            doc.Save();
        }

        public void CreateTemplate(string path)
        {
            using (WordprocessingDocument doc = WordprocessingDocument.Create(path, WordprocessingDocumentType.Document))
            {
                InitializeMainPart(doc);
                InsertParts(doc);
            }
        }

        private void InitializeMainPart(WordprocessingDocument doc)
        {
            doc.AddMainDocumentPart();
            doc.MainDocumentPart.Document = new Document { Body = new Body() };
        }


        private void Initialize(WordprocessingDocument doc)
        {
            var stylesPart = doc.MainDocumentPart.AddNewPart<StyleDefinitionsPart>();
            styles = new Styles
            {
                DocDefaults = CreateDocdefaults(),
                LatentStyles = CreateLatentStyles(),
            };
            styles.Save(stylesPart);
        }

        public DocDefaults CreateDocdefaults()
        {
            return new DocDefaults
            {
                RunPropertiesDefault = new RunPropertiesDefault
                {
                    RunPropertiesBaseStyle = new RunPropertiesBaseStyle
                    {
                        Color = new Color
                        {
                            Val = "red"
                        },
                        RunFonts = new RunFonts
                        {
                            Ascii = "Times New Roman",
                            ComplexScript = "Times New Roman",
                            HighAnsi = "Times New Roman",
                        },
                    }
                },
            };
        }

        public LatentStyles CreateLatentStyles()
        {
            return new LatentStyles
            {
                DefaultLockedState = true,
                DefaultSemiHidden = true,
                DefaultUnhideWhenUsed =true,
                DefaultPrimaryStyle = false,
            };
        }

        private void AddStyles(WordprocessingDocument doc)
        {
            AddAbsatzStyle();
            AddTitelStyle();
            AddUntertitelStyle();
            AddBilanzsummeStyle();
            AddZwischentitelStyle();
            AddBerichtsteilueberschriftStyle();
            //AddOrderedListStyle();
            //AddUnorderedListStyle();
            AddTableHeadStyle();
            AddTableBodyStyle();
            AddUnterzeichnerStyle();
            AddDontConvertStyle();
            styles.Save(doc.MainDocumentPart.StyleDefinitionsPart);
        }

        private void AddAbsatzStyle()
        {
            var rPrA = new StyleRunProperties
            {
                Color = new Color { Val = "black" },
                FontSize = new FontSize { Val = "20" },
            };
            CreateStyle(
                BanzXml.A,
                BanzXml.parNameMap[BanzXml.A],
                true,
                BanzXml.A,
                rPrA,
                both);
        }

        private void AddTitelStyle()
        {
            var rPrTitel = new StyleRunProperties
            {
                Color = new Color { Val = "black" },
                FontSize = new FontSize { Val = "28" },
                Bold = new Bold(),
            };
            CreateStyle(
                BanzXml.Titel,
                BanzXml.parNameMap[BanzXml.Titel],
                false,
                BanzXml.A,
                rPrTitel,
                center);
        }

        private void AddUntertitelStyle()
        {
            var rPrUntertitel = new StyleRunProperties
            {
                Color = new Color { Val = "black" },
                FontSize = new FontSize { Val = "28" },
                Bold = new Bold(),
            };
            CreateStyle(
                BanzXml.Untertitel,
                BanzXml.parNameMap[BanzXml.Untertitel],
                false,
                BanzXml.A,
                rPrUntertitel,
                center);
        }

        private void AddBilanzsummeStyle()
        {
            var rPrBilanzsumme = new StyleRunProperties
            {
                Color = new Color { Val = "black" },
                FontSize = new FontSize { Val = "20" },
            };
            CreateStyle(
                BanzXml.Bilanzsumme,
                BanzXml.parNameMap[BanzXml.Bilanzsumme],
                false,
                BanzXml.Bilanzsumme,
                rPrBilanzsumme,
                both);
        }

        private void AddZwischentitelStyle()
        {
            var rPrZwischentitel = new StyleRunProperties
            {
                Color = new Color { Val = "black" },
                FontSize = new FontSize { Val = "20" },
                Bold = new Bold(),
            };
            CreateStyle(
                BanzXml.Zwischentitel,
                BanzXml.parNameMap[BanzXml.Zwischentitel],
                false,
                BanzXml.A,
                rPrZwischentitel,
                left);
        }

        private void AddBerichtsteilueberschriftStyle()
        {
            var rPrBerichtsteilueberschrift = new StyleRunProperties
            {
                Color = new Color { Val = "black" },
                FontSize = new FontSize { Val = "22" },
                Bold = new Bold(),
            };
            CreateStyle(
                BanzXml.BerichtsteilUeberschrift,
                BanzXml.parNameMap[BanzXml.BerichtsteilUeberschrift],
                false,
                BanzXml.A,
                rPrBerichtsteilueberschrift,
                left);
        }

        private void AddTableHeadStyle()
        {
            var rPrTHead = new StyleRunProperties
            {
                Color = new Color { Val = "black" },
                FontSize = new FontSize { Val = "20" },
                Bold = new Bold(),
            };
            CreateStyle(
                BanzXml.THEAD,
                BanzXml.parNameMap[BanzXml.THEAD],
                false,
                BanzXml.THEAD,
                rPrTHead,
                left);
        }

        private void AddTableBodyStyle()
        {
            var rPrTBody = new StyleRunProperties
            {
                Color = new Color { Val = "black" },
                FontSize = new FontSize { Val = "20" },
            };
            CreateStyle(
                BanzXml.TBODY,
                BanzXml.parNameMap[BanzXml.TBODY],
                false,
                BanzXml.TBODY,
                rPrTBody,
                left);
        }

        private void AddUnterzeichnerStyle()
        {
            var rPrUnterzeichner = new StyleRunProperties
            {
                Color = new Color { Val = "black" },
                FontSize = new FontSize { Val = "20" },
            };
            CreateStyle(
                BanzXml.Unterzeichner,
                BanzXml.parNameMap[BanzXml.Unterzeichner],
                false,
                BanzXml.Unterzeichner,
                rPrUnterzeichner,
                left);
        }

        private void AddDontConvertStyle()
        {
            var dontConvert = new StyleRunProperties
            {
                Color = new Color { Val = "black" },
                FontSize = new FontSize { Val = "20" },
            };
            CreateStyle(
                BanzXml.DontConvert,
                BanzXml.parNameMap[BanzXml.DontConvert],
                false,
                BanzXml.DontConvert,
                dontConvert,
                left);
        }


        private Style CreateStyle(string styleId, string styleName, bool defaultParagraph, string next,
            StyleRunProperties rPr, JustificationValues justification)
        {
            var style = new Style
            {
                Type = StyleValues.Paragraph,
                StyleId = styleId,
                StyleName = new StyleName { Val = styleName },
                PrimaryStyle = new PrimaryStyle{Val = OnOffOnlyValues.On},
                CustomStyle = true,
                Default = defaultParagraph,
                NextParagraphStyle = new NextParagraphStyle { Val = next },
                StyleRunProperties = rPr,
                StyleParagraphProperties = new StyleParagraphProperties { Justification = new Justification{ Val = justification}},
            };
            styles.Append(style);
            return style;
        }
    }
}