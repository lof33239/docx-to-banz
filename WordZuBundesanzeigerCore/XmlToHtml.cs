﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using System.Linq;
using WordZuBundesanzeigerCore.util;
namespace WordZuBundesanzeigerCore
{
    public class XmlToHtml
    {
        static readonly XElement colgroup = new XElement("colgroup");


        private static readonly Dictionary<string, XElement> xml_html = new Dictionary<string, XElement>()
        {
            {BanzXml.A,new XElement ("p")  },
            {BanzXml.Bekanntmachungskopf,new XElement ("div") },
            {BanzXml.BerichtsteilUeberschrift,new XElement("h3", new XAttribute("class","b_teil"))},
            {BanzXml.Bilanzsumme,new XElement ("p") },
            {BanzXml.Bold, new XElement("span", new XAttribute("style", "font-weight:bold;"))},
            {BanzXml.BoldItalic, new XElement("span", new XAttribute("style", "font-weight:bold;font-style:italic;"))},
            {BanzXml.COL,new XElement("col") },
            {BanzXml.UnternehmensregisterPublikation,new XElement("td") },
            {BanzXml.eBundesanzeigerPublikation,new XElement("td") },
            {BanzXml.Firmenname,new XElement ("h3",new XAttribute("class","z_titel") ) },
            {BanzXml.Firmensitz,new XElement ("h4" ,new XAttribute("class","z_titel"))},
            {BanzXml.Italic, new XElement("span", new XAttribute("style", "font-style:italic;"))},
            {BanzXml.LA,new XElement ("td" ,new XAttribute("class","list_content"))},
            {BanzXml.LI,new XElement("tr") },
            {BanzXml.nZ,new XElement("br") },
            {BanzXml.OL,new XElement("table",new XAttribute("class","list_table_ol"),new XAttribute("cellpadding","0mm")) },
            {BanzXml.Signatur,new XElement("div") },
            {BanzXml.TABLE,new XElement("table",new XAttribute("class","std_table"),new XAttribute("width","900"),new XAttribute("cellspacing","0"),new XAttribute("cellpadding","2"),new XAttribute("border","0"))},
            {BanzXml.TBODY,new XElement("tbody") },
            {BanzXml.TD,new XElement("td") },
            {BanzXml.THEAD,new XElement("thead") },
            {BanzXml.Titel,new XElement("h3" ,new XAttribute("class","l_titel"))},
            {BanzXml.TR,new XElement("tr",new XAttribute("style","background-color: transparent;"))},
            {BanzXml.UL,new XElement("table",new XAttribute("class","list_table_ul"),new XAttribute("cellpadding","0mm")) },
            {BanzXml.Untertitel,new XElement("h4",new XAttribute("class","z_titel"))},
            {BanzXml.Unterzeichner,new XElement("p") },
            {BanzXml.Zwischentitel,new XElement("h4",new XAttribute("class","z_titel"))},
            {"Fussnotenbereich",new XElement("div")},
            {"Fussnote",new XElement("div")},
            {"FnZeichen",new XElement("sup")},
            {"FnAbsatz",new XElement("p")},
            {"FnRef",new XElement("sup")},
        };

        const string head =
@"<head>
    <meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8""/>
    <meta name=""format-detection"" content=""telephone=no""/>
    <link rel=""shortcut icon"" href=""https://www.bundesanzeiger.de/images/favicons/banz.ico""/>
    <link rel=""stylesheet"" type=""text/css"" href=""Bundesanzeiger-Dateien/main.css"" media=""screen""/>
    <link rel=""stylesheet"" type=""text/css"" href=""Bundesanzeiger-Dateien/de.css"" media=""screen""/>
    <link rel=""stylesheet"" type=""text/css"" href=""Bundesanzeiger-Dateien/eb_main.css"" media=""screen""/>
    <link rel=""stylesheet"" type=""text/css"" href=""Bundesanzeiger-Dateien/publication.css""/>
    <link rel=""stylesheet"" type=""text/css"" href=""Bundesanzeiger-Dateien/annual.css"" media=""screen""/>
    <link rel=""stylesheet"" type=""text/css"" href=""Bundesanzeiger-Dateien/browser_ff_win.css"" media=""screen""/>
    <link rel=""stylesheet"" type=""text/css"" href=""Bundesanzeiger-Dateien/print.css"" media=""print""/>
    <style type=""text/css"">
                <!--
                span a.help_link_sign { display: none; }
                body { background: transparent; }
                -->
            </style>
    <script type=""text/javascript"" async="""" defer=""defer"" src=""Bundesanzeiger-Dateien/piwik.js""></script><script type=""text/javascript"" language=""javascript"">
        var htdocs = '/ebanzwww/..';
        var css = '/ebanzwww/../css';
        var images = '/ebanzwww/../images';
        var path = '/ebanzwww/wexsservlet';
        var wexsservlet_url = 'https://publikations-plattform.de/sp/wexsservlet';
        var contentloader_url = '/ebanzwww/contentloader';
        var session = 'session.sessionid=8fed39bf62c57f8cb39819f0aa600fc6';
        var search_field_placeholder = 'ISIN, WKN, Fondsname, Gesellschaft';
        var show_concern_txt = 'Konzernunterlagen anzeigen';
var hide_concern_txt = 'Konzernunterlagen verbergen';
var show_subsidiary_txt = 'Befreite Unternehmen anzeigen';
var hide_subsidiary_txt = 'Befreite Unternehmen verbergen';
</script><script type=""text/javascript"" src=""Bundesanzeiger-Dateien/jquery.js""></script><script type=""text/javascript"" src=""Bundesanzeiger-Dateien/main.js""></script><title>Bundesanzeiger</title>
</head>"
;

        static readonly XElement head_h = XElement.Parse(head);



        public static XDocument GetHtml(XDocument xml)
        {
            var tr = new XElement("tr");
            var ret = new XDocument(
                new XDocumentType("html", null, null, null),
                new XElement("html",
                    head_h,
                    new XElement("body",
                        new XAttribute("style", "background-color: #f0f0f0;"),
                        new XElement("div",
                            new XAttribute("id", "container"),
                            new XElement("table",
                                new XAttribute("class", "layout"),
                                new XAttribute("style", "background-image:none;"),
                                new XElement("tbody",
                                    new XElement("tr",
                                        new XAttribute("id", "tr_content"),
                                        new XElement("td",
                                            new XAttribute("id", "td_content"),
                                            new XAttribute("class", "no_pad"),
                                            new XAttribute("colspan", "3"),
                                            new XElement("div",
                                                new XAttribute("style", "background-color:white;"),
                                                new XAttribute("class", "frame_text"),
                                                new XElement("div",
                                                    new XAttribute("id", "preview_data"),
                                                    new XAttribute("class", "publicationcontent"),
                                                    new XElement("div",
                                                        new XAttribute("class", "forcemargin"),
                                                        " "),
                                                    new XElement("div",
                                                        new XElement("table",
                                                            new XAttribute("id", "begin_pub"),
                                                            new XElement("tbody",
                                                            tr)
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            );

            var inner_html = GetHtml(xml.Root);
            if (null != inner_html)
                tr.Add(inner_html);

            MoveColInColgroup(tr);
            HandleUnorderedLists(tr);
            HandleOrderedLists(tr);

            return ret;
        }

        private static void MoveColInColgroup(XElement tr)
        {
            tr.Descendants("table")
                .Where(x => x.Elements("col").Any())
                .Select(x =>
                {
                    XElement cg = new XElement(colgroup);
                    x.AddFirst(cg);
                    cg.ElementsAfterSelf("col").Select(col =>
                    {
                        col.Remove();
                        cg.Add(col);
                        return col;
                    });
                    return x;
                }
                );
        }

        private static void HandleUnorderedLists(XElement tr)
        {
            var tds = tr.Descendants("table")
                .Where(x => "list_table_ul".Equals(x.Attributes("class").First().Value))
                .Elements("tr")
                .Elements("td")
                .ToList();
            foreach (var td in tds)
            {

                td.AddBeforeSelf(new XElement("td", new XAttribute("class", "list_ident"), td.Descendants().Any() ? "" : "-"));
            }
        }

        private static void HandleOrderedLists(XElement tr)
        {
            var tds = tr.Descendants("table")
                .Where(x => "list_table_ol".Equals(x.Attributes("class").First().Value))
                .Elements("tr")
                .Elements("td")
                .ToList();
            foreach (var td in tds)
            {
                var i = td.Parent.ElementsBeforeSelf()
                    .Where(row => !row.Elements("td").Descendants().Any())
                    .Count() + 1;
                td.AddBeforeSelf(new XElement("td", new XAttribute("class", "list_ident"), td.Descendants().Any() ? "" : $"{i}."));
            }
        }

        private static XElement GetHtml(XElement xml)
        {
            if (!xml_html.ContainsKey(xml.Name.LocalName))
                return null;
            HandleRowColors(xml.Name.LocalName);
            XElement htmlElement = new XElement(xml_html[xml.Name.LocalName]);

            if (xml.Name.LocalName == "Fussnote")
            {
                var zeichen = GetHtml(xml.Elements().First());
                var absatz = GetHtml(xml.Elements().Skip(1).First());
                absatz.AddFirst(zeichen);
                htmlElement.Add(absatz);
                return htmlElement;
            }

            if (xml.Name.LocalName == "FnRef")
            {
                htmlElement.Add(new XText(xml.Attribute("FnRefID").Value));
                return htmlElement;
            }

            htmlElement.Add(TransformAttributes(xml));
            foreach (var node in xml.Nodes())
            {
                if (node is XText) { htmlElement.Add(node); }
                else if (node is XElement xElement)
                {
                    var child = GetHtml(xElement);
                    if (null != child) { htmlElement.Add(child); }
                }
            }
            return htmlElement;
        }

        private static XAttribute[] TransformAttributes(XElement xml)
        {
            return xml.Attributes()
                .Select(attr =>
                {
                    switch (attr.Name.LocalName)
                    {
                        case BanzXml.ATTR_COLSPAN:
                            return new XAttribute(BanzXml.ATTR_COLSPAN, attr.Value);
                        case BanzXml.ATTR_ALIGN:
                            return new XAttribute(BanzXml.ATTR_ALIGN, attr.Value);
                        case BanzXml.ATTR_WIDTH:
                            return new XAttribute("style", $"width: {attr.Value};");
                    }
                    return null;
                })
                .Where(attr => attr != null)
                .ToArray();
        }

        private static void HandleRowColors(string localName)
        {
            switch (localName)
            {
                case BanzXml.THEAD:
                    xml_html[BanzXml.TR].Attribute("style").Value = "background-color: #cdcdce;";
                    break;
                case BanzXml.TBODY:
                    xml_html[BanzXml.TR].Attribute("style").Value = "background-color: #f0f0f0;";
                    break;
                case BanzXml.TR:
                    var v = xml_html[BanzXml.TR].Attribute("style").Value;
                    if (v == "background-color: transparent;")
                        xml_html[BanzXml.TR].Attribute("style").Value = "background-color: #f0f0f0;";
                    else if (v == "background-color: #f0f0f0;")
                        xml_html[BanzXml.TR].Attribute("style").Value = "background-color: transparent;";
                    break;
            }
        }


    }
}
