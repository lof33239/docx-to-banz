﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using WordZuBundesanzeigerCore.util;

namespace WordZuBundesanzeigerCore
{
    public static class ExtensionMethods
    {
        private static Regex anythingButAscii = new Regex("[^ -~]", RegexOptions.Compiled);
        public static XElement ToNamespace(this XElement element, XNamespace xmlNamespace)
        {
            foreach (var node in element.DescendantsAndSelf())
            {
                node.Name = xmlNamespace + node.Name.LocalName;

            }
            return element;
        }

        public static void RenameTo(this XElement element, string newName)
        {
            element.Name = element.Name.Namespace + newName;
        }

        public static string GetWithAsciiKeys(this IDictionary<string,string> dict, string key)
        {
            try
            {
                return dict[key];
            }
            catch (KeyNotFoundException)
            {
                foreach(var _key in dict.Keys)
                {

                    if (key.Equals(anythingButAscii.Replace(_key, "")))
                        return dict[_key];
                }
                throw;
            }
        }
    }
}
