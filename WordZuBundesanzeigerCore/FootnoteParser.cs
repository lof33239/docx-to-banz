﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace WordZuBundesanzeigerCore
{
    internal class FootnoteParser
    {
        private readonly WordprocessingDocument wordDocument;
        private readonly ILogger log;
        private readonly BundesanzeigerXmlCreator xmlCreator;
        private readonly XElement footNoteBereich;
        private readonly HashSet<long> ids = new HashSet<long>();

        public FootnoteParser(WordprocessingDocument wordDocument, ILogger log, BundesanzeigerXmlCreator xmlCreator)
        {
            this.wordDocument = wordDocument;
            this.log = log;
            this.xmlCreator = xmlCreator;
            this.footNoteBereich = xmlCreator.CreateXElement("Fussnotenbereich");
        }

        public XElement FootNoteBereich => footNoteBereich;
        public bool HasFootnotes => ids.Count > 0;

        internal void addFootnoteRef(FootnoteReference refer, XElement currentXmlNode)
        {
            var id = refer?.Id;
            if (id == null)
            {
                log.LogError("Tried to add footnote without id");
                throw new Exception("Bad");
            }
            var footNoteRef = xmlCreator.CreateXElement("FnRef", new XAttribute("FnRefID",id));
            currentXmlNode.Add(footNoteRef);

            if (ids.Contains(id.Value))
            {
                return;
            }

            var viableFootnotes = wordDocument.MainDocumentPart.FootnotesPart.Footnotes.Elements<Footnote>().Where(f => f?.Id == id).Take(1).ToArray();
            if (viableFootnotes.Length != 1)
            {
                log.LogError("Wrong length of found footnotes <>1");
                throw new Exception("Wrong length of found footnotes <>1");
            }
            ids.Add(id.Value);
            var footnote = viableFootnotes[0];
            
            footNoteBereich.Add(
                xmlCreator.CreateXElement("Fussnote",
                    new XAttribute("FnID", id),
                    xmlCreator.CreateXElement("FnZeichen", id),
                    xmlCreator.CreateXElement("FnAbsatz", footnote.InnerText)
                )
            );
        }
    }
}