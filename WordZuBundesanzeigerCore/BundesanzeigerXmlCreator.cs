﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace WordZuBundesanzeigerCore

{
    public sealed class BundesanzeigerXmlCreator
    {
        private readonly XElement head;

        public BundesanzeigerXmlCreator(string corporationName, string corporationLocation, XmlVariant variant)
        {
            xmlVariant = variant;
            XmlNamespace = xmlVariant == XmlVariant.Bundesanzeiger ? BanzXml.BanzNamespace : BanzXml.UnternehmensregisterNamespace;

            head = CreateHead();
            AddToHead(CreateXElement(BanzXml.Firmenname, corporationName));
            AddToHead(CreateXElement(BanzXml.Firmensitz, corporationLocation));

            
            XDocument = new XDocument(CreateRoot(xmlVariant));
            XDocument.Declaration = new XDeclaration("1.0", "UTF-8", null);

            AddToRoot(head);
        }

        public readonly XNamespace XmlNamespace;
        public readonly XmlVariant xmlVariant;

        public XDocument XDocument { get; }

        private XElement CreateRoot(XmlVariant variant)
        {
            XNamespace xsi = @"http://www.w3.org/2001/XMLSchema-instance";

            if (variant == XmlVariant.Bundesanzeiger) return CreateBundesanzeigerRoot(xsi);
            else return CreateUnternehmensregisterRoot(xsi);
        }

        private XElement CreateBundesanzeigerRoot(XNamespace xsi)
        {
            return
                CreateXElement(
                    BanzXml.eBundesanzeigerPublikation,
                    new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                    new XAttribute(xsi + "schemaLocation", $"{XmlNamespace} {BanzXml.BanzSchema}"),
                    new XAttribute("Bereich", BanzXml.BanzBereich),
                    new XAttribute("Version", "1")
                );
        }

        private XElement CreateUnternehmensregisterRoot(XNamespace xsi)
        {
            return
                CreateXElement(
                    BanzXml.UnternehmensregisterPublikation,
                    new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                    new XAttribute(xsi + "schemaLocation", $"{XmlNamespace} {BanzXml.UnternehmensregisterSchema}"),
                    new XAttribute("Bereich", BanzXml.UnternehmensregisterBereich),
                    new XAttribute("Version", "1")
                );
        }

        private XElement CreateHead()
        {
            return CreateXElement(BanzXml.Bekanntmachungskopf);
        }

        public XElement CreateXElement(string nodeName) => new XElement(XmlNamespace + nodeName);

        public XElement CreateXElement(string nodeName, params object[] content) => new XElement(XmlNamespace + nodeName, content);

        public override string ToString() => XDocument.Declaration.ToString() + Environment.NewLine + XDocument.ToString();

        public void AddToRoot(XElement node) => XDocument.Root.Add(node);

        public void AddToHead(XElement node)
        {
            head.Add(node);
        }

        public void Validate()
        {
            XmlSchemaSet schemas = new XmlSchemaSet();
            var schemaString = xmlVariant == XmlVariant.Bundesanzeiger ? Properties.Resources.BundesanzeigerPublikation : Properties.Resources.UnternehmensregisterPublikation;
            schemas.Add(XmlNamespace.NamespaceName, XmlReader.Create(new StringReader(schemaString)));
            XDocument.Validate(schemas, (sender, e) => throw new XmlSchemaValidationException(e.Message, e.Exception), true);
        }
    }
}
