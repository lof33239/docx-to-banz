﻿using System;

namespace WordZuBundesanzeigerCore
{
    public class WordParseException : Exception
    {

        public WordParseException(string message) : base(message) { ParagraphCaret = -1; }

        public WordParseException(string message, int caret) : base(message) {
            ParagraphCaret = caret;
        }

        public int ParagraphCaret { get; }
    }
}