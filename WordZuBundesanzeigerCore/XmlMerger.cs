﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.Linq;

namespace WordZuBundesanzeigerCore
{
    public class XmlMerger
    {
        private readonly ILogger logger;
        public XmlMerger(ILogger logger) {
            this.logger = logger;
        }

        private IEnumerable<XElement> GetElements(XDocument doc)
        {
            var bekanntmachungskopf = doc.Root.Elements().First(el => el.Name.LocalName == BanzXml.Bekanntmachungskopf);
            var titel = bekanntmachungskopf.Elements().First(el => el.Name.LocalName == BanzXml.Titel);
            titel.RenameTo(BanzXml.BerichtsteilUeberschrift);
            var ueberschrift = titel;

            var elements = doc.Root.Elements().Where(el => el.Name.LocalName != BanzXml.Bekanntmachungskopf).ToList();
            elements.Insert(0, ueberschrift);

            return elements;
        }

        private List<XElement> GetElementsFromFiles(string[] fromFiles)
        {
            List<XElement> elements = new List<XElement>();
            foreach (var filepath in fromFiles)
            {
                var xdoc = XDocument.Load(filepath);
                elements.AddRange(GetElements(xdoc));
            }

            return elements;
        }

        private List<XElement> ToNamespace(List<XElement> elements, XNamespace xnamespace)
        {
            return elements.Select(e => e.ToNamespace(xnamespace)).ToList();
        }

        public XDocument Append(string[] fromFiles, XDocument toDocument)
        {
            logger.Log($"Appending files");
            var appendElements = ToNamespace(GetElementsFromFiles(fromFiles), toDocument.Root.Name.Namespace);
            toDocument.Root.Add(appendElements);
            logger.Log($"Appending files done");
            return toDocument;

        }
        public XDocument Prepend(string[] fromFiles, XDocument toDocument)
        {
            logger.Log($"Prepending files");
            List<XElement> prependElements = ToNamespace(GetElementsFromFiles(fromFiles), toDocument.Root.Name.Namespace);
           
            toDocument.Root
                .Elements()
                .First(el => el.Name.LocalName != "Bekanntmachungskopf")
                .AddBeforeSelf(prependElements);
            logger.Log($"Prepending files done");
            return toDocument;

        }
    }
}
