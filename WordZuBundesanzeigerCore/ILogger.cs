﻿using System;

namespace WordZuBundesanzeigerCore
{
    public interface ILogger
    {
        void Log(string message);
        void LogWarning(string message);
        void LogError(string message, Exception exception = null);
    }

    public sealed class CompositeLogger : ILogger
    {
        private readonly ILogger[] loggers;

        public CompositeLogger(params ILogger[] loggers)
        {
            this.loggers = loggers;
        }

        public void Log(string message)
        {
            foreach (var logger in loggers)
            {
                logger.Log(message);
            }
        }

        public void LogError(string message, Exception exception = null)
        {
            foreach (var logger in loggers)
            {
                logger.LogError(message, exception);
            }
        }

        public void LogWarning(string message)
        {
            foreach (var logger in loggers)
            {
                logger.LogWarning(message);
            }
        }
    }

    public sealed class SerilogLoggingAdapter : ILogger
    {
        private readonly Serilog.ILogger serilogLogger;

        public SerilogLoggingAdapter(Serilog.ILogger serilogLogger)
        {
            this.serilogLogger = serilogLogger;
        }

        public void Log(string message)
        {
            serilogLogger.Information(message);
        }

        public void LogError(string message, Exception exception = null)
        {
            serilogLogger.Error(exception, message);
        }

        public void LogWarning(string message)
        {
            serilogLogger.Warning(message);
        }
    }


    public sealed class DiagnosticsDebugLogger : ILogger
    {

        public void Log(string message)
        {
            System.Diagnostics.Debug.WriteLine(message);
        }

        public void LogError(string message, Exception exception = null)
        {
            Log("Error: " + message);
        }

        public void LogWarning(string message)
        {
            Log("Warning: " + message);
        }
    }

    public sealed class MockLogger : ILogger
    {
        public void Log(string message) { }

        public void LogError(string message, Exception exception = null) { }

        public void LogWarning(string message) { }
    }
}