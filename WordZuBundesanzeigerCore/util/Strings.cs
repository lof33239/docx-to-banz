﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WordZuBundesanzeigerCore.util
{
    public class Strings
    {
        public const string unordered_xml =
@"<w:abstractNum w:abstractNumId=""1"" w15:restartNumberingAfterBreak=""0""
xmlns:w15=""http://schemas.microsoft.com/office/word/2012/wordml""
xmlns:w=""http://schemas.openxmlformats.org/wordprocessingml/2006/main"" >
    <w:multiLevelType w:val=""hybridMultilevel""/>
    <w:lvl w:ilvl=""0"">
        <w:lvlRestart w:val=""0""/> 
        <w:start w:val=""1""/>
        <w:numFmt w:val=""bullet""/>
        <w:lvlText w:val=""""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""720"" w:hanging=""360""/>
        </w:pPr>
        <w:rPr>
            <w:rFonts w:ascii=""Symbol"" w:hAnsi=""Symbol"" w:hint=""default""/>
        </w:rPr>
    </w:lvl>
    <w:lvl w:ilvl=""1"" w:tentative=""0"">
        <w:start w:val=""1""/>
        <w:numFmt w:val=""bullet""/>
        <w:lvlText w:val=""o""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""1440"" w:hanging=""360""/>
        </w:pPr>
        <w:rPr>
            <w:rFonts w:ascii=""Courier New"" w:hAnsi=""Courier New"" w:cs=""Courier New"" w:hint=""default""/>
        </w:rPr>
    </w:lvl>
    <w:lvl w:ilvl=""2"" w:tentative=""0"">
        <w:start w:val=""1""/>
        <w:numFmt w:val=""bullet""/>
        <w:lvlText w:val=""""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""2160"" w:hanging=""360""/>
        </w:pPr>
        <w:rPr>
            <w:rFonts w:ascii=""Wingdings"" w:hAnsi=""Wingdings"" w:hint=""default""/>
        </w:rPr>
    </w:lvl>
    <w:lvl w:ilvl=""3"" w:tentative=""0"">
        <w:start w:val=""1""/>
        <w:numFmt w:val=""bullet""/>
        <w:lvlText w:val=""""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""2880"" w:hanging=""360""/>
        </w:pPr>
        <w:rPr>
            <w:rFonts w:ascii=""Symbol"" w:hAnsi=""Symbol"" w:hint=""default""/>
        </w:rPr>
    </w:lvl>
    <w:lvl w:ilvl=""4"" w:tentative=""0"">
        <w:start w:val=""1""/>
        <w:numFmt w:val=""bullet""/>
        <w:lvlText w:val=""o""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""3600"" w:hanging=""360""/>
        </w:pPr>
        <w:rPr>
            <w:rFonts w:ascii=""Courier New"" w:hAnsi=""Courier New"" w:cs=""Courier New"" w:hint=""default""/>
        </w:rPr>
    </w:lvl>
    <w:lvl w:ilvl=""5"" w:tentative=""0"">
        <w:start w:val=""1""/>
        <w:numFmt w:val=""bullet""/>
        <w:lvlText w:val=""""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""4320"" w:hanging=""360""/>
        </w:pPr>
        <w:rPr>
            <w:rFonts w:ascii=""Wingdings"" w:hAnsi=""Wingdings"" w:hint=""default""/>
        </w:rPr>
    </w:lvl>
    <w:lvl w:ilvl=""6"" w:tentative=""0"">
        <w:start w:val=""1""/>
        <w:numFmt w:val=""bullet""/>
        <w:lvlText w:val=""""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""5040"" w:hanging=""360""/>
        </w:pPr>
        <w:rPr>
            <w:rFonts w:ascii=""Symbol"" w:hAnsi=""Symbol"" w:hint=""default""/>
        </w:rPr>
    </w:lvl>
    <w:lvl w:ilvl=""7"" w:tentative=""0"">
        <w:start w:val=""1""/>
        <w:numFmt w:val=""bullet""/>
        <w:lvlText w:val=""o""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""5760"" w:hanging=""360""/>
        </w:pPr>
        <w:rPr>
            <w:rFonts w:ascii=""Courier New"" w:hAnsi=""Courier New"" w:cs=""Courier New"" w:hint=""default""/>
        </w:rPr>
    </w:lvl>
    <w:lvl w:ilvl=""8"" w:tentative=""0"">
        <w:start w:val=""1""/>
        <w:numFmt w:val=""bullet""/>
        <w:lvlText w:val=""""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""6480"" w:hanging=""360""/>
        </w:pPr>
        <w:rPr>
            <w:rFonts w:ascii=""Wingdings"" w:hAnsi=""Wingdings"" w:hint=""default""/>
        </w:rPr>
    </w:lvl>
</w:abstractNum>";

        public const string ordered_xml = @"<w:abstractNum w:abstractNumId=""0"" w15:restartNumberingAfterBreak=""1""
xmlns:w15=""http://schemas.microsoft.com/office/word/2012/wordml""
xmlns:w=""http://schemas.openxmlformats.org/wordprocessingml/2006/main"" >
    <w:multiLevelType w:val=""hybridMultilevel""/>
    <w:lvl w:ilvl=""0"">
        <w:start w:val=""1""/>
        <w:numFmt w:val=""decimal""/>
        <w:lvlText w:val=""%1.""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""720"" w:hanging=""360""/>
        </w:pPr>
    </w:lvl>
    <w:lvl w:ilvl=""1"" w:tentative=""0"">
        <w:start w:val=""1""/>
        <w:numFmt w:val=""decimal""/>
        <w:lvlText w:val=""%2.""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""1440"" w:hanging=""360""/>
        </w:pPr>
    </w:lvl>
    <w:lvl w:ilvl=""2"" w:tentative=""0"">
        <w:start w:val=""1""/>
        <w:numFmt w:val=""decimal""/>
        <w:lvlText w:val=""%3.""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""2160"" w:hanging=""360""/>
        </w:pPr>
    </w:lvl>
    <w:lvl w:ilvl=""3"" w:tentative=""0"">
        <w:start w:val=""1""/>
        <w:numFmt w:val=""decimal""/>
        <w:lvlText w:val=""%4.""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""2880"" w:hanging=""360""/>
        </w:pPr>
    </w:lvl>
    <w:lvl w:ilvl=""4"" w:tentative=""0"">
        <w:start w:val=""1""/>
        <w:numFmt w:val=""decimal""/>
        <w:lvlText w:val=""%5.""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""3600"" w:hanging=""360""/>
        </w:pPr>
    </w:lvl>
    <w:lvl w:ilvl=""5"" w:tentative=""0"">
        <w:start w:val=""1""/>
        <w:numFmt w:val=""decimal""/>
        <w:lvlText w:val=""%6.""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""4320"" w:hanging=""360""/>
        </w:pPr>
    </w:lvl>
    <w:lvl w:ilvl=""6"" w:tentative=""0"">
        <w:start w:val=""1""/>
        <w:numFmt w:val=""decimal""/>
        <w:lvlText w:val=""%7.""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""5040"" w:hanging=""360""/>
        </w:pPr>
    </w:lvl>
    <w:lvl w:ilvl=""7"" w:tentative=""0"">
        <w:start w:val=""1""/>
        <w:numFmt w:val=""decimal""/>
        <w:lvlText w:val=""%8.""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""5760"" w:hanging=""360""/>
        </w:pPr>
    </w:lvl>
    <w:lvl w:ilvl=""8"" w:tentative=""0"">
        <w:start w:val=""1""/>
        <w:numFmt w:val=""decimal""/>
        <w:lvlText w:val=""%9.""/>
        <w:lvlJc w:val=""left""/>
        <w:pPr>
            <w:ind w:left=""6480"" w:hanging=""360""/>
        </w:pPr>
    </w:lvl>
</w:abstractNum>";
    }
}
