﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WordZuBundesanzeigerCore.util
{
    public static class Util
    {
        public static string GetAssemblyDirectory()
        {
            return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }

        public static string GetAppdataDirectory()
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                "WordZuBundesanzeiger");
        }

        public static string GetLogFilePath()
        {
            return Path.Combine(Util.GetAppdataDirectory(), "log.txt");
        }

        public static string GetTempfileName()
        {
            return Path.Combine(Path.GetTempPath(), Path.GetRandomFileName().Replace('.', '_'));
        }

        public static void Add<T>(this Queue<T> queue, T obj)
        {
            queue.Enqueue(obj);
        }

        public static T Get<T>(this Queue<T> queue)
        {
            return queue.Dequeue();
        }

        public static XmlVariant GetXmlVariantByLabel(string label)
        {
            if (label == "Bundesanzeiger") return XmlVariant.Bundesanzeiger;
            else if (label == "Unternehmensregister") return XmlVariant.Unternehmensregister;
            throw new Exception("Unknown XmlVariant Label");
        }
    }
}