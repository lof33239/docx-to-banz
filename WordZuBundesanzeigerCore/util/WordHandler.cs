﻿//using DocumentFormat.OpenXml;
//using DocumentFormat.OpenXml.Packaging;
//using DocumentFormat.OpenXml.Wordprocessing;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace WordZuBundesanzeigerCore.util
//{
//    class WordHandler
//    {
//        public static void AddComment(OpenXmlCompositeElement element, WordprocessingDocument document,string comment)
//        {
//            AddComment(element, document, "Bundesanzeiger Addin", "BA", comment);
//        }
//        // Insert a comment on the first paragraph.
//        public static void AddComment(OpenXmlCompositeElement element, WordprocessingDocument document,
//            string author, string initials, string comment)
//        {
//            Comments comments = null;
//            string id = "0";

//            // Verify that the document contains a 
//            // WordProcessingCommentsPart part; if not, add a new one.
//            if (document.MainDocumentPart.GetPartsOfType<WordprocessingCommentsPart>().Count() > 0)
//            {
//                comments = document.MainDocumentPart.WordprocessingCommentsPart.Comments;
//                if (comments.HasChildren)
//                {
//                    // Obtain an unused ID.
//                    id = (comments.Descendants<Comment>().Select(e => int.Parse(e.Id.Value)).Max()+1).ToString();
//                }
//            }
//            else
//            {
//                // No WordprocessingCommentsPart part exists, so add one to the package.
//                WordprocessingCommentsPart commentPart =
//                    document.MainDocumentPart.AddNewPart<WordprocessingCommentsPart>();
//                commentPart.Comments = new Comments();
//                comments = commentPart.Comments;
//            }

//            // Compose a new Comment and add it to the Comments part.
//            Paragraph p = new Paragraph(new Run(new Text(comment)));
//            Comment cmt =
//                new Comment()
//                {
//                    Id = id,
//                    Author = author,
//                    Initials = initials,
//                    Date = DateTime.Now
//                };
//            cmt.AppendChild(p);
//            comments.AppendChild(cmt);
//            comments.Save();

//            // Specify the text range for the Comment. 
//            // Insert the new CommentRangeStart before the first run of paragraph.
//            element.InsertBefore(new CommentRangeStart()
//            { Id = id }, element.GetFirstChild<Run>());

//            // Insert the new CommentRangeEnd after last run of paragraph.
//            var cmtEnd = element.InsertAfter(new CommentRangeEnd()
//            { Id = id }, element.Elements<Run>().Last());

//            // Compose a run with CommentReference and insert it.
//            element.InsertAfter(new Run(new CommentReference() { Id = id }), cmtEnd);
//            document.Save();
//        }
//    }
//}
