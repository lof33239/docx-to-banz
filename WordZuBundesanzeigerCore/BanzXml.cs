﻿using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using WordZuBundesanzeigerCore.util;

namespace WordZuBundesanzeigerCore
{
    public enum XmlVariant
    {
        Bundesanzeiger,
        Unternehmensregister,
    }
    public class BanzXml : XDocument
    {
        public static readonly XNamespace ns = @"http://www.ebundesanzeiger.de/publikation/layout";
        //TODO add this construct private readonly XDocument xDocument;
        private static XmlSchemaSet schemaSet;

        public static XmlSchemaSet SchemaSet
        {
            get
            {
                if (schemaSet is null)
                {
                    schemaSet = new XmlSchemaSet();
                    schemaSet.Add("http://www.ebundesanzeiger.de/publikation/layout", XmlReader.Create("BundesanzeigerPublikation.xsd"));
                }
                return schemaSet;
            }
        }
        public BanzXml(string firmenname, string firmensitz) : base(new XDeclaration("1.0", "utf-8", null))
        {
            XNamespace xsi = @"http://www.w3.org/2001/XMLSchema-instance";
            Add(new XElement(ns + "eBundesanzeigerPublikation",
                new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                new XAttribute(xsi + "schemaLocation",
                    @"http://www.ebundesanzeiger.de/publikation/layout ebundesanzeigerpublikation.xsd"),
                new XAttribute("Bereich", "Rechnungslegung-Finanzberichte"),
                new XAttribute("Version", "1"))
            );

            Root.AddFirst(new XElement(ns + "Bekanntmachungskopf",
                new XElement(ns + "Firmenname", firmenname),
                new XElement(ns + "Firmensitz", firmensitz)
            ));

        }

        public static BanzXml GetMinimalTestBuaXml(string firmenname, string firmensitz)
        {
            BanzXml b = new BanzXml(firmenname, firmensitz);
            b.Descendants().First(x => "Bekanntmachungskopf" == x.Name.LocalName).Add(new XElement(ns + "Titel", "Titel"));
            return b;
        }

        public void AddToRoot(object content)
        {
            if (content is XElement element)
            {
                foreach (var n in element.DescendantsAndSelf())
                {
                    n.Name = ns + n.Name.LocalName;
                }
            }

            Root.Add(content);
        }


        //public override string ToString()
        //{
        //    if (null != Declaration)
        //        return Declaration.ToString() + Environment.NewLine + base.ToString();
        //    else
        //        return  base.ToString();
        //}

        //Absatztypen
        public const string
            A = "A",
            BanzBereich = "Rechnungslegung-Finanzberichte",
            BanzNamespace = @"http://www.ebundesanzeiger.de/publikation/layout",
            BanzSchema = "ebundesanzeigerpublikation.xsd",
            Bekanntmachungskopf = "Bekanntmachungskopf",
            BerichtsteilUeberschrift = "BerichtsteilUeberschrift",
            Bilanzsumme = "Bilanzsumme",
            Bold = "b",
            BoldItalic = "bi",
            COL = "COL",
            DontConvert = "Nicht_uebernehmen",
            eBundesanzeigerPublikation = "eBundesanzeigerPublikation",
            Firmenname = "Firmenname",
            Firmensitz = "Firmensitz",
            Italic = "i",
            LA = "LA",
            LI = "LI",
            nZ = "nZ",
            OL = "OL",
            Signatur = "Signatur",
            TABLE = "TABLE",
            TBODY = "TBODY",
            TD = "TD",
            THEAD = "THEAD",
            Titel = "Titel",
            TR = "TR",
            UL = "UL",
            Untertitel = "Untertitel",
            Unterzeichner = "Unterzeichner",
            Zwischentitel = "Zwischentitel";
        public const string
            UnternehmensregisterPublikation = "UnternehmensregisterPublikation",
            UnternehmensregisterBereich = "Unternehmensregister",
            UnternehmensregisterNamespace = "http://www.unternehmensregister.de/publikation/layout",
            UnternehmensregisterSchema = "unternehmensregisterpublikation.xsd";


        //Alignment für Tabellenspalten
        public const string
            ALIGN_LEFT = "left",
            ALIGN_CENTER = "center",
            ALIGN_RIGHT = "right";

        //Attributnamen
        public const string
            ATTR_ALIGN = "align",
            ATTR_VALIGN = "valign",
            ATTR_COLSPAN = "colspan",
            ATTR_WIDTH = "width";

        public static BiDictionary<string, string> parNameMap = new BiDictionary<string, string>
        {
            {Titel,"__Titel__"},
            {Bilanzsumme,Bilanzsumme },
            {Untertitel,"__Untertitel__" },
            {A, "Absatz"},
            {BerichtsteilUeberschrift, "BerichtsteilÜberschrift"},
            {Zwischentitel, Zwischentitel},
            {OL, "geordnete Liste"},
            {UL, "ungeordnete Liste"},
            {THEAD, "Tabellenkopf"},
            {TBODY, "Tabellenkörper"},
            {Unterzeichner, Unterzeichner },
            {DontConvert,DontConvert },
        };

        public static string JustMap(JustificationValues j)
        {
            switch (j)
            {
                case var a when a == JustificationValues.Left:
                case var b when b == JustificationValues.Start:
                case var c when c == JustificationValues.Both:
                    return ALIGN_LEFT;
                case var a when a == JustificationValues.Center:
                    return ALIGN_CENTER;
                case var a when a == JustificationValues.Right:
                case var b when b == JustificationValues.End:
                    return ALIGN_RIGHT;
                default:
                    return ALIGN_LEFT;
            }
        }
    }
}
